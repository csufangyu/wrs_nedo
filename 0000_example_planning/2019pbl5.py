from pandaplotutils import pandactrl as pandactrl
import manipulation.grip.robotiq85.robotiq85 as rtq85
import robotsim.ur3dual.ur3dual as robot
import robotsim.ur3dual.ur3dualmesh as robotmesh
import utiltools.robotmath as rm
import numpy as np
import environment.collisionmodel as cm

if __name__=="__main__":

    base = pandactrl.World(camp = [3000,0,5000], lookatp = [0,0,700])

    hndfa = rtq85.Robotiq85Factory()
    rtq85 = hndfa.genHand()
    base.pggen.plotAxis(rtq85.handnp, length=15, thickness=2)
    prejawwidth, prehndfc, prehndmat4 = (rtq85.approachAt(0,0,60,0,1,0,-1,0,0,jawwidth=60))

    rgthnd = hndfa.genHand()
    lfthnd = hndfa.genHand()

    rbt = robot.Ur3DualRobot(rgthnd, lfthnd)
    rbt.goinitpose()
    rbtmg = robotmesh.Ur3DualMesh()
    rbtmg.genmnp(rbt, togglejntscoord=False).reparentTo(base.render)

    objpos=np.array([300,-400,1100])
    objrot=np.dot(rm.rodrigues([0,0,1],45), rm.rodrigues([0,1,0],-135))
    print(objrot)
    objmat4=rm.homobuild(objpos, objrot)

    obj = cm.CollisionModel(objinit="./objects/domino.stl")
    obj.setColor(.8, .6, .3, .5)
    obj.setMat(base.pg.np4ToMat4(objmat4))
    obj.reparentTo(base.render)

    hndmat4 = np.dot(objmat4, prehndmat4)
    eepos = rm.homotransform(objmat4, prehndfc)[:3]
    eerot = hndmat4[:3,:3]
    print(eepos, eerot)

    rtq85new = hndfa.genHand()
    rtq85new.setColor(0,1,0,.5)
    rtq85new.setMat(base.pg.np4ToMat4(hndmat4))
    rtq85new.setJawwidth(prejawwidth)
    rtq85new.reparentTo(base.render)

    armname = "rgt"
    armjnts = rbt.numik(eepos, eerot, armname)
    rbt.movearmfk(armjnts, armname)
    rbt.closegripper(armname, prejawwidth)
    rbtmg.genmnp(rbt, togglejntscoord=False, rgbargt=[0, 1, 0.0, .5]).reparentTo(base.render)

    base.run()
import os
import pandaplotutils.pandactrl as pandactrl
import robotsim.nextage.nxt as robotsim
import robotsim.nextage.nxtmesh as robotmesh
import manipulation.grip.robotiq85.rtq85nm as rtq85nm
import environment.collisionmodel as cm

class Env(object):

    def __init__(self, betransparent=False):
        """
        load obstacles model
        separated by category

        :param base:
        author: weiwei
        date: 20181205
        """

        self.__this_dir, this_filename = os.path.split(__file__)

        # table
        self.__tablepath = os.path.join(self.__this_dir, "obstacles", "nxttable.stl")
        self.__tablecm = cm.CollisionModel(self.__tablepath, betransparent)
        self.__tablecm.setColor(.6,.6,.6,1.0)
        self.__tablecm.setPos(190.0, 0.0, 0.0)

        # shelf
        self.__framerightupheight = os.path.join(self.__this_dir, "obstacles", "bldgfframeupheight.stl")
        self.__framerightlowheight = os.path.join(self.__this_dir, "obstacles", "bldgfframelowheight.stl")
        self.__framerightdepth = os.path.join(self.__this_dir, "obstacles", "bldgfframedepth.stl")
        self.__framefrontbottom = os.path.join(self.__this_dir, "obstacles", "bldgfframewidth.stl")

        # front
        self.__framefrontrightupheightcm = cm.CollisionModel(self.__framerightupheight)
        self.__framefrontrightupheightcm.setColor(.6,.6,.6,1.0)
        self.__framefrontrightupheightcm.setPos(600.0+190.0+10.0+30.0, -580.0, 1800.0)
        self.__framefrontleftupheightcm = cm.CollisionModel(self.__framerightupheight)
        self.__framefrontleftupheightcm.setColor(.6,.6,.6,1.0)
        self.__framefrontleftupheightcm.setPos(600.0+190.0+10.0+30.0, 580.0, 1800.0)
        self.__framefrontrightlowheightcm = cm.CollisionModel(self.__framerightlowheight)
        self.__framefrontrightlowheightcm.setColor(.6,.6,.6,1.0)
        self.__framefrontrightlowheightcm.setPos(600.0+190.0+10.0+30.0, -580.0, 0.0)
        self.__framefrontleftlowheightcm = cm.CollisionModel(self.__framerightlowheight)
        self.__framefrontleftlowheightcm.setColor(.6,.6,.6,1.0)
        self.__framefrontleftlowheightcm.setPos(600.0+190.0+10.0+30.0, 580.0, 0.0)

        self.__framefrontbottomcm = cm.CollisionModel(self.__framefrontbottom)
        self.__framefrontbottomcm.setColor(.6,.6,.6,1.0)
        self.__framefrontbottomcm.setPos(600.0+190.0+10.0+30.0, -550.0, 730.0)
        self.__framefrontmiddlecm = cm.CollisionModel(self.__framefrontbottom)
        self.__framefrontmiddlecm.setColor(.6,.6,.6,1.0)
        self.__framefrontmiddlecm.setPos(600.0+190.0+10.0+30.0, -550.0, 1830.0)
        self.__framefronttopcm = cm.CollisionModel(self.__framefrontbottom)
        self.__framefronttopcm.setColor(.6,.6,.6,1.0)
        self.__framefronttopcm.setPos(600.0+190.0+10.0+30.0, -550.0, 2170.0)
        self.__framefronthangcm = cm.CollisionModel(self.__framefrontbottom)
        self.__framefronthangcm.setColor(.6,.6,.6,1.0)
        self.__framefronthangcm.setPos(300.0+190.0+10.0+30.0, -550.0, 2170.0)

        # back
        self.__framebackrightupheightcm = cm.CollisionModel(self.__framerightupheight)
        self.__framebackrightupheightcm.setColor(.6,.6,.6,1.0)
        self.__framebackrightupheightcm.setPos(600.0+190.0+10.0+30.0-1600-60, -580.0, 1800.0)
        self.__framebackleftupheightcm = cm.CollisionModel(self.__framerightupheight)
        self.__framebackleftupheightcm.setColor(.6,.6,.6,1.0)
        self.__framebackleftupheightcm.setPos(600.0+190.0+10.0+30.0-1600-60, 580.0, 1800.0)
        self.__framebackrightlowheightcm = cm.CollisionModel(self.__framerightlowheight)
        self.__framebackrightlowheightcm.setColor(.6,.6,.6,1.0)
        self.__framebackrightlowheightcm.setPos(600.0+190.0+10.0+30.0-1600-60, -580.0, 0.0)
        self.__framebackleftlowheightcm = cm.CollisionModel(self.__framerightlowheight)
        self.__framebackleftlowheightcm.setColor(.6,.6,.6,1.0)
        self.__framebackleftlowheightcm.setPos(600.0+190.0+10.0+30.0-1600-60, 580.0, 0.0)

        self.__framebackbottomcm = cm.CollisionModel(self.__framefrontbottom)
        self.__framebackbottomcm.setColor(.6,.6,.6,1.0)
        self.__framebackbottomcm.setPos(600.0+190.0+10.0+30.0-1600-60, -550.0, 730.0)
        self.__framebackmiddlecm = cm.CollisionModel(self.__framefrontbottom)
        self.__framebackmiddlecm.setColor(.6,.6,.6,1.0)
        self.__framebackmiddlecm.setPos(600.0+190.0+10.0+30.0-1600-60, -550.0, 1830.0)
        self.__framebacktopcm = cm.CollisionModel(self.__framefrontbottom)
        self.__framebacktopcm.setColor(.6,.6,.6,1.0)
        self.__framebacktopcm.setPos(600.0+190.0+10.0+30.0-1600-60, -550.0, 2170.0)

        # depth
        self.__framerightbottomcm = cm.CollisionModel(self.__framerightdepth)
        self.__framerightbottomcm.setColor(.6,.6,.6,1.0)
        self.__framerightbottomcm.setPos(600.0+190.0+10.0, -580.0, 130.0)
        self.__framerightmiddlecm = cm.CollisionModel(self.__framerightdepth)
        self.__framerightmiddlecm.setColor(.6,.6,.6,1.0)
        self.__framerightmiddlecm.setPos(600.0+190.0+10.0, -580.0, 1830.0)
        self.__framerightmiddle2cm = cm.CollisionModel(self.__framerightdepth)
        self.__framerightmiddle2cm.setColor(.6,.6,.6,1.0)
        self.__framerightmiddle2cm.setPos(600.0+190.0+10.0, -580.0, 730.0)
        self.__framerightupcm = cm.CollisionModel(self.__framerightdepth)
        self.__framerightupcm.setColor(.6,.6,.6,1.0)
        self.__framerightupcm.setPos(600.0+190.0+10.0, -580.0, 2170.0)

        self.__frameleftbottomcm = cm.CollisionModel(self.__framerightdepth)
        self.__frameleftbottomcm.setColor(.6,.6,.6,1.0)
        self.__frameleftbottomcm.setPos(600.0+190.0+10.0, 580.0, 130.0)
        self.__frameleftmiddlecm = cm.CollisionModel(self.__framerightdepth)
        self.__frameleftmiddlecm.setColor(.6,.6,.6,1.0)
        self.__frameleftmiddlecm.setPos(600.0+190.0+10.0, 580.0, 1830.0)
        self.__frameleftupcm = cm.CollisionModel(self.__framerightdepth)
        self.__frameleftupcm.setColor(.6,.6,.6,1.0)
        self.__frameleftupcm.setPos(600.0+190.0+10.0, 580.0, 2170.0)

        self.__battached = False
        self.__changableobslist = []

    def reparentTo(self, nodepath):
        if not self.__battached:
            # table
            self.__tablecm.reparentTo(nodepath)
            # shelf
            self.__framefrontrightupheightcm.reparentTo(nodepath)
            self.__framefrontleftupheightcm.reparentTo(nodepath)
            self.__framefrontrightlowheightcm.reparentTo(nodepath)
            self.__framefrontleftlowheightcm.reparentTo(nodepath)
            self.__framefrontbottomcm.reparentTo(nodepath)
            self.__framefrontmiddlecm.reparentTo(nodepath)
            self.__framefronttopcm.reparentTo(nodepath)
            self.__framefronthangcm.reparentTo(nodepath)
            self.__framebackrightupheightcm.reparentTo(nodepath)
            self.__framebackleftupheightcm.reparentTo(nodepath)
            self.__framebackrightlowheightcm.reparentTo(nodepath)
            self.__framebackleftlowheightcm.reparentTo(nodepath)
            self.__framebackbottomcm.reparentTo(nodepath)
            self.__framebackmiddlecm.reparentTo(nodepath)
            self.__framebacktopcm.reparentTo(nodepath)
            self.__framerightbottomcm.reparentTo(nodepath)
            self.__framerightmiddlecm.reparentTo(nodepath)
            self.__framerightmiddle2cm.reparentTo(nodepath)
            self.__framerightupcm.reparentTo(nodepath)
            self.__frameleftbottomcm.reparentTo(nodepath)
            self.__frameleftmiddlecm.reparentTo(nodepath)
            self.__frameleftupcm.reparentTo(nodepath)
            self.__battached = True

    def loadobj(self, name):
        self.__objpath = os.path.join(self.__this_dir, "objects", name)
        self.__objcm = cm.CollisionModel(self.__objpath, type="ball")
        return self.__objcm
    
    def getstationaryobslist(self):
        """
        generate the collision model for stationary obstacles

        :return:

        author: weiwei
        date: 20180811
        """

        stationaryobslist = [self.__tablecm, self.__framefrontrightupheightcm, self.__framefrontleftupheightcm,
                             self.__framefrontrightlowheightcm, self.__framefrontleftlowheightcm,
                             self.__framefrontbottomcm, self.__framefrontmiddlecm, self.__framefronttopcm,
                             self.__framefronthangcm,
                             self.__framebackrightupheightcm, self.__framebackleftupheightcm,
                             self.__framebackrightlowheightcm, self.__framebackleftlowheightcm,
                             self.__framebackbottomcm, self.__framebackmiddlecm, self.__framebacktopcm,
                             self.__framerightbottomcm, self.__framerightmiddlecm, self.__framerightmiddle2cm, self.__framerightupcm,
                             self.__frameleftbottomcm, self.__frameleftmiddlecm, self.__frameleftupcm]
        return stationaryobslist

    def getchangableobslist(self):
        """
        get the collision model for changable obstacles

        :return:

        author: weiwei
        date: 20190313
        """
        return self.__changableobslist

    def addchangableobs(self, nodepath, objcm, pos, rot):
        """

        :param objcm: CollisionModel
        :param pos: nparray 1x3
        :param rot: nparray 3x3
        :return:

        author: weiwei
        date: 20190313
        """

        self.__changableobslist.append(objcm)
        objcm.reparentTo(nodepath)
        objcm.setMat(base.pg.npToMat4(rot, pos))

    def removechangableobs(self, objcm):
        if objcm in self.__changableobslist:
            objcm.remove()

if __name__ == '__main__':
    import utiltools.robotmath as rm
    import numpy as np
    from panda3d.core import *

    base = pandactrl.World(camp=[2700,300,2700], lookatp=[0,0,1000])
    env = Env()
    env.reparentTo(base.render)
    objcm = env.loadobj("bunnysim.stl")

    objcm.setColor(.2,.5,0,1)
    objcm.setPos(400,-200,1200)
    objcm.reparentTo(base.render)
    objcm.showcn()
    obscmlist = env.getstationaryobslist()
    for obscm in obscmlist:
        obscm.showcn()

    objpos = np.array([400,-300,1200])
    objrot = rm.rodrigues([0,1,0], 45)
    objcm2 = env.loadobj("housing.stl")
    objcm2.setColor(1,.5,0,1)
    env.addchangableobs(base.render, objcm2, objpos, objrot)

    robotsim = robotsim.NxtRobot()
    rgthnd = rtq85nm.newHand(hndid = "rgt", ftsensoroffset=0)
    lfthnd = rtq85nm.newHand(hndid = "lft", ftsensoroffset=0)
    robotmeshgen = robotmesh.NxtMesh(rgthand = rgthnd, lfthand = lfthnd)
    robotmesh = robotmeshgen.genmnp(robotsim, toggleendcoord=False)
    robotmesh.reparentTo(base.render)

    base.run()
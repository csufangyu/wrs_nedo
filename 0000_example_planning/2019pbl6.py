from pandaplotutils import pandactrl as pandactrl
import manipulation.grip.robotiq85.robotiq85 as rtq85
import robotsim.ur3dual.ur3dual as robot
import robotsim.ur3dual.ur3dualmesh as robotmesh
import robotsim.ur3dual.ur3dualball as robotball
import utiltools.robotmath as rm
import numpy as np
import environment.collisionmodel as cm
import bunrisettingfree as bsf
import motionplanning.collisioncheckerball as cdball
import environment.bulletcdhelper as bch

# def getAvailableGrasps(idlist, predefined_graspss, objpos, objrot, obscmlist, rbt, rbtball):


if __name__=="__main__":

    base = pandactrl.World(camp = [5000,-3000,3000], lookatp = [0,0,700])

    env = bsf.Env(betransparent=True)
    obscmlist = env.getstationaryobslist()
    for obscm in obscmlist:
        obscm.setColor(.3,.3,.3,.5)
    env.reparentTo(base.render)
    obscmlist = env.getstationaryobslist()

    hndfa = rtq85.Robotiq85Factory()
    rtq85 = hndfa.genHand()
    base.pggen.plotAxis(rtq85.handnp, length=15, thickness=2)
    predefined_graspss = []
    predefined_graspss.append(rtq85.approachAt(0,0,90,0,1,0,-.707,0,-.707,jawwidth=60))
    predefined_graspss.append(rtq85.approachAt(0,0,90,0,1,0,.707,0,-.707,jawwidth=60))
    predefined_graspss.append(rtq85.approachAt(0,0,90,0,1,0,-.707,0,.707,jawwidth=60))
    predefined_graspss.append(rtq85.approachAt(0,0,90,0,1,0,.707,0,.707,jawwidth=60))
    predefined_graspss.append(rtq85.approachAt(0,0,90,0,1,0,-1,0,0,jawwidth=60))
    predefined_graspss.append(rtq85.approachAt(0,0,90,0,1,0,1,0,0,jawwidth=60))
    predefined_graspss.append(rtq85.approachAt(0,0,30,0,1,0,-.707,0,.707,jawwidth=60))
    predefined_graspss.append(rtq85.approachAt(0,0,30,0,1,0,.707,0,.707,jawwidth=60))
    predefined_graspss.append(rtq85.approachAt(0,0,30,0,1,0,-.707,0,-.707,jawwidth=60))
    predefined_graspss.append(rtq85.approachAt(0,0,30,0,1,0,.707,0,-.707,jawwidth=60))
    predefined_graspss.append(rtq85.approachAt(0,0,30,0,1,0,-1,0,0,jawwidth=60))
    predefined_graspss.append(rtq85.approachAt(0,0,30,0,1,0,1,0,0,jawwidth=60))
    # predefined_graspss.append(rtq85.approachAt(0,0,90,0,-1,0,-.707,0,-.707,jawwidth=60))
    # predefined_graspss.append(rtq85.approachAt(0,0,90,0,-1,0,.707,0,-.707,jawwidth=60))
    # predefined_graspss.append(rtq85.approachAt(0,0,90,0,-1,0,-1,0,0,jawwidth=60))
    # predefined_graspss.append(rtq85.approachAt(0,0,90,0,-1,0,1,0,0,jawwidth=60))
    # predefined_graspss.append(rtq85.approachAt(0,0,30,0,-1,0,-.707,0,.707,jawwidth=60))
    # predefined_graspss.append(rtq85.approachAt(0,0,30,0,-1,0,.707,0,.707,jawwidth=60))
    # predefined_graspss.append(rtq85.approachAt(0,0,30,0,-1,0,-1,0,0,jawwidth=60))
    # predefined_graspss.append(rtq85.approachAt(0,0,30,0,-1,0,1,0,0,jawwidth=60))

    rgthnd = hndfa.genHand()
    lfthnd = hndfa.genHand()

    rbt = robot.Ur3DualRobot(rgthnd, lfthnd)
    rbt.goinitpose()
    rbtmg = robotmesh.Ur3DualMesh()
    rbtmg.genmnp(rbt, togglejntscoord=False).reparentTo(base.render)

    rbtball = robotball.Ur3DualBall()
    pcdchecker = cdball.CollisionCheckerBall(rbtball)
    bcdchecker = bch.MCMchecker(toggledebug=False)
    bcdchecker.showMeshList(obscmlist)

    # start pose
    objpos=np.array([450,-100,1050])
    objrot=np.dot(rm.rodrigues([0,0,1],90), rm.rodrigues([0,1,0],-90))
    objrot=np.dot(rm.rodrigues([0,0,1],-30), rm.rodrigues([0,1,0],-90))
    print(objrot)
    objmat4=rm.homobuild(objpos, objrot)

    obj = cm.CollisionModel(objinit="./objects/domino.stl")
    obj.setColor(.8, .6, .3, .5)
    obj.setColor(1, 0, 0, .5)
    obj.setMat(base.pg.np4ToMat4(objmat4))
    # obj.reparentTo(base.render)
    bcdchecker.showMesh(obj)

    availablePre = []
    for idpre, predefined_grasp in enumerate(predefined_graspss):
        prejawwidth, prehndfc, prehndmat4 = predefined_grasp
        hndmat4 = np.dot(objmat4, prehndmat4)
        eepos = rm.homotransform(objmat4, prehndfc)[:3]
        eerot = hndmat4[:3,:3]
        print(eepos, eerot)

        rtq85new = hndfa.genHand()
        rtq85new.setMat(base.pg.np4ToMat4(hndmat4))
        rtq85new.setJawwidth(prejawwidth)
        isHndCollided = bcdchecker.isMeshListMeshListCollided(rtq85new.cmlist, obscmlist)
        print(isHndCollided)
        if not isHndCollided:
            armname = "rgt"
            armjnts = rbt.numik(eepos, eerot, armname)
            if armjnts is not None:
                rtq85new = hndfa.genHand()
                rtq85new.setColor(0, 1, 0, .5)
                # rtq85new.setColor(.3, .3, .3, .5)
                rtq85new.setMat(base.pg.np4ToMat4(hndmat4))
                rtq85new.setJawwidth(prejawwidth)
                # rtq85new.reparentTo(base.render)
                bcdchecker.showMeshList(rtq85new.cmlist)
                rbt.movearmfk(armjnts, armname)
                rbt.closegripper(armname, prejawwidth)
                isRbtCollided = pcdchecker.isRobotCollided(rbt, obscmlist, holdarmname=armname)
                isObjCollided = pcdchecker.isObjectsOthersCollided([obj], rbt, armname, obscmlist)
                print(isRbtCollided, isObjCollided)
                if (not isRbtCollided) and (not isObjCollided):
                    # rbtball = robotball.Ur3DualBall()
                    # rbtball.showcn(rbtball.genholdbcndict(rbt, armname))
                    # rbtmg.genmnp(rbt, drawhand=False, togglejntscoord=False, rgbargt=[0, 1, 0, .5]).reparentTo(base.render)
                    availablePre.append(idpre)
                elif (not isObjCollided):
                    # rbtmg.genmnp(rbt, drawhand=False, togglejntscoord=False, rgbargt=[1, 0, 1, .5]).reparentTo(base.render)
                    pass
                else:
                    pass
            else:
                rtq85new = hndfa.genHand()
                rtq85new.setColor(1, .6, 0, .5)
                # rtq85new.setColor(.3, .3, .3, .5)
                rtq85new.setMat(base.pg.np4ToMat4(hndmat4))
                rtq85new.setJawwidth(prejawwidth)
                # rtq85new.reparentTo(base.render)
                bcdchecker.showMeshList(rtq85new.cmlist)
        else:
            rtq85new = hndfa.genHand()
            rtq85new.setColor(1, 0, 1, .5)
            # rtq85new.setColor(.3, .3, .3, .5)
            rtq85new.setMat(base.pg.np4ToMat4(hndmat4))
            rtq85new.setJawwidth(prejawwidth)
            # rtq85new.reparentTo(base.render)
            bcdchecker.showMeshList(rtq85new.cmlist)

    # goal pose
    objpos=np.array([400,-300,1050])
    objrot=rm.rodrigues([0,0,1],-135)
    print(objrot)
    objmat4=rm.homobuild(objpos, objrot)

    obj = cm.CollisionModel(objinit="./objects/domino.stl")
    obj.setColor(.8, .6, .3, .5)
    obj.setColor(0, 0, 1, .5)
    obj.setMat(base.pg.np4ToMat4(objmat4))
    obj.reparentTo(base.render)

    for idpre in availablePre:
        prejawwidth, prehndfc, prehndmat4 = predefined_graspss[idpre]
        hndmat4 = np.dot(objmat4, prehndmat4)
        eepos = rm.homotransform(objmat4, prehndfc)[:3]
        eerot = hndmat4[:3,:3]
        print(eepos, eerot)
        rtq85new = hndfa.genHand()
        rtq85new.setMat(base.pg.np4ToMat4(hndmat4))
        rtq85new.setJawwidth(prejawwidth)
        isHndCollided = bcdchecker.isMeshListMeshListCollided(rtq85new.cmlist, obscmlist)
        print(isHndCollided)
        if not isHndCollided:
            armname = "rgt"
            armjnts = rbt.numik(eepos, eerot, armname)
            if armjnts is not None:
                rtq85new = hndfa.genHand()
                rtq85new.setColor(0, 1, 0, .5)
                # rtq85new.setColor(.3, .3, .3, .5)
                rtq85new.setMat(base.pg.np4ToMat4(hndmat4))
                rtq85new.setJawwidth(prejawwidth)
                rtq85new.reparentTo(base.render)
                bcdchecker.showMeshList(rtq85new.cmlist)
                rbt.movearmfk(armjnts, armname)
                rbt.closegripper(armname, prejawwidth)
                isRbtCollided = pcdchecker.isRobotCollided(rbt, obscmlist, holdarmname=armname)
                isObjCollided = pcdchecker.isObjectsOthersCollided([obj], rbt, armname, obscmlist)
                print(isRbtCollided, isObjCollided)
                if (not isRbtCollided) and (not isObjCollided):
                    rbtmg.genmnp(rbt, togglejntscoord=False, rgbargt=[0, 1, 0, .5]).reparentTo(base.render)
                    print(idpre)
                    pass
                elif (not isObjCollided):
                    rbtmg.genmnp(rbt, drawhand=False, togglejntscoord=False, rgbargt=[1, 0, 1, .5]).reparentTo(base.render)
                    pass
                else:
                    pass
            else:
                rtq85new = hndfa.genHand()
                rtq85new.setColor(1, .6, 0, .5)
                # rtq85new.setColor(.3, .3, .3, .5)
                rtq85new.setMat(base.pg.np4ToMat4(hndmat4))
                rtq85new.setJawwidth(prejawwidth)
                rtq85new.reparentTo(base.render)
                bcdchecker.showMeshList(rtq85new.cmlist)
        else:
            rtq85new = hndfa.genHand()
            rtq85new.setColor(1, 0, 1, .5)
            # rtq85new.setColor(.3, .3, .3, .5)
            rtq85new.setMat(base.pg.np4ToMat4(hndmat4))
            rtq85new.setJawwidth(prejawwidth)
            rtq85new.reparentTo(base.render)
            bcdchecker.showMeshList(rtq85new.cmlist)

    base.run()
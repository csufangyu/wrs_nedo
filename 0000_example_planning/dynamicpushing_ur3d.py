import robotsim.ur3dual.ur3dual as ur3dual
from robotsim.ur3dual import ur3dualmesh
from robotsim.ur3dual import ur3dualball
import bunrisettingfree
from pandaplotutils import pandactrl
import manipulation.grip.robotiq85.robotiq85 as rtq85
from dynasim import bdmodel as bdm
import os
import utiltools.robotmath as rm
import trimesh
import numpy as np
from panda3d.core import Vec3

if __name__=="__main__":
    base = pandactrl.World(camp=[100, 0, 3000], lookatp=[300, 0, 1000])

    objname = "tool_motordriver.stl"
    env = bunrisettingfree.Env()
    env.reparentTo(base.render)
    obscmlist = env.getstationaryobslist()

    hndfa = rtq85.Robotiq85Factory()
    rgthnd = hndfa.genHand()
    lfthnd = hndfa.genHand()
    robot = ur3dual.Ur3DualRobot(rgthnd, lfthnd)
    robot.opengripper(armname="rgt")
    robot.closegripper(armname="lft")
    robotball = ur3dualball.Ur3DualBall()
    robotmesh = ur3dualmesh.Ur3DualMesh()
    robotnp = robotmesh.genmnp(robot)
    robotnp.reparentTo(base.render)

    this_dir, this_filename = os.path.split(__file__)
    objpath = os.path.join(this_dir, "objects", "bigpush.stl")
    objbm = bdm.BDModel(objpath, mass=150, dynamic = True, friction=.2, shapetype="convex")
    objbm.setColor(.3,.5,.7,1)
    objbm.setPos(300,0,1040)

    # table
    plane = trimesh.primitives.Box(box_extents=[1000, 1000, 100], box_center=[500, 0, -50])
    planebm = bdm.BDModel(plane, dynamic=False, friction=0.3)
    planebm.setColor(.5,.5,.5,1)
    planebm.setPos(0,0,1035)

    stick = trimesh.primitives.Box(box_extents=[1000, 20, 20], box_center=[-500, 0, 0])
    stickbm = bdm.BDModel(stick, mass=0, dynamic = True, shapetype="convex")
    stickbm.setColor(.1,.1,.1,1)
    stickrot = rm.rodrigues([0,0,1],0)
    stickpos = np.array([170,0,1200])
    stickbm.sethomomat(rm.homobuild(stickpos, stickrot))

    base.attachRUD(objbm, planebm, stickbm)

    def update(stickbm, task):
        if base.inputmgr.keyMap['w'] is True:
            sticknpmat4 = stickbm.gethomomat()
            sticknpmat4[0,3] += .5
            stickbm.sethomomat(sticknpmat4)
        if base.inputmgr.keyMap['s'] is True:
            sticknpmat4 = stickbm.gethomomat()
            sticknpmat4[0,3] -= .5
            stickbm.sethomomat(sticknpmat4)
        if base.inputmgr.keyMap['a'] is True:
            sticknpmat4 = stickbm.gethomomat()
            sticknpmat4[1,3] += 1
            stickbm.sethomomat(sticknpmat4)
        if base.inputmgr.keyMap['d'] is True:
            sticknpmat4 = stickbm.gethomomat()
            sticknpmat4[1,3] -= 1
            stickbm.sethomomat(sticknpmat4)
        return task.cont


    taskMgr.add(update, extraArgs=[stickbm], appendTask=True)
    base.run()
"""
This file shows how to perform motion planning.
You need to declare three objects to initialize a motion planner:
1. a collisioncheckerball object
2. a ctcallback object
3. a motion planner (could be rrt, ddrrt, rrtconnect(recommended), ddrrtconnect, etc.

You may also declare a smoother object to smooth the path using random cut afterwards.

author: weiwei, toyonaka
date: 20190312
"""

from motionplanning import smoother as sm
from motionplanning import ctcallback as ctcb
from motionplanning import collisioncheckerball as cdck
from motionplanning.rrt import rrtconnect as rrtc
from robotsim.ur3dual import ur3dual
from robotsim.ur3dual import ur3dualmesh
from robotsim.ur3dual import ur3dualball
from pandaplotutils import pandactrl
import manipulation.grip.robotiq85.robotiq85 as rtq85
import bunrisettingfree
import numpy as np
import copy
import utiltools.robotmath as rm

base = pandactrl.World(camp=[2700, 300, 2700], lookatp=[0, 0, 1000])

objname = "tool_motordriver.stl"
env = bunrisettingfree.Env()
env.reparentTo(base.render)
obscmlist = env.getstationaryobslist()
# for obscm in obscmlist:
#     obscm.showcn()

hndfa = rtq85.Robotiq85Factory()
rgthnd = hndfa.genHand()
lfthnd = hndfa.genHand()
robot = ur3dual.Ur3DualRobot(rgthnd, lfthnd)
robot.opengripper(armname="rgt")
robot.closegripper(armname="lft")
robotball = ur3dualball.Ur3DualBall()
robotmesh = ur3dualmesh.Ur3DualMesh()
robotnp = robotmesh.genmnp(robot)
# robotnp.reparentTo(base.render)
armname = 'rgt'
cdchecker = cdck.CollisionCheckerBall(robotball)
ctcallback = ctcb.CtCallback(base, robot, cdchecker, armname=armname)
smoother = sm.Smoother()

robot.goinitpose()
# robotnp = robotmesh.genmnp(robot, jawwidthrgt = 85.0, jawwidthlft=0.0)
# robotnp.reparentTo(base.render)
# base.run()

# load obj collision model using env
objcm = env.loadobj("bunnysim.stl")
# another example -- load obj collision model independently
# this_dir, this_filename = os.path.split(__file__)
# objname = "tool_motordriver.stl"
# objpath = os.path.join(this_dir, "objects", objname)
# objcm = cm.CollisionModel(objpath)
objpos = np.array([370,-200,1100])
objrot = np.array([[1.0,0.0,0.0],
                    [0.0,1.0,0.0],
                    [0.0,0.0,1.0]]).T
objcmcopys = copy.deepcopy(objcm)
objcmcopys.setColor(0.0,0.0,1.0,.3)
objcmcopys.setMat(base.pg.npToMat4(npmat3=objrot,npvec3=objpos))
objcmcopys.reparentTo(base.render)
objcmcopys.showcn()
obscmlist.append(objcmcopys)

starttreesamplerate = 25
endtreesamplerate = 30
rbtstartpos = np.array([350,-250,1100])
rbtstartrot = np.array([[1,0,0],
                    [0,-0.92388,-0.382683],
                    [0,0.382683,-0.92388]]).T
rbtstartpos = rbtstartpos - 100.0*rbtstartrot[:,2]

rbtgoalpos = np.array([455,-100,1100])
rbtgoalrot = np.dot(rm.rodrigues([0,0,1],-120),rbtstartrot)
rbtgoalpos = rbtgoalpos - 100.0*rbtgoalrot[:,2]
start = robot.numik(rbtstartpos, rbtstartrot, armname=armname)
print(start)
goal = robot.numik(rbtgoalpos, rbtgoalrot, armname=armname)
print(goal)
planner = rrtc.RRTConnect(start=start, goal=goal, ctcallback=ctcallback,
                              starttreesamplerate=starttreesamplerate,
                              endtreesamplerate=endtreesamplerate, expanddis=5,
                              maxiter=2000, maxtime=100.0)
robot.movearmfk(start, armname)
robotnp = robotmesh.genmnp(robot)
robotnp.reparentTo(base.render)
robotball.showcn(robotball.genfullbcndict(robot))
robot.movearmfk(goal, armname)
robotnp = robotmesh.genmnp(robot)
robotnp.reparentTo(base.render)
# base.run()
robot.goinitpose()
[path, sampledpoints] = planner.planning(obscmlist)
path = smoother.pathsmoothing(path, planner)
print(path)

# for pose in path:
#     robot.movearmfk(pose, armname)
#     robotstick = robotmesh.gensnp(robot = robot)
#     robotstick.reparentTo(base.render)
# base.run()

def update(rbtmnp, motioncounter, robot, path, armname, robotmesh, robotball, task):
    if motioncounter[0] < len(path):
        if rbtmnp[0] is not None:
            rbtmnp[0].detachNode()
            # rbtmnp[1].detachNode()
        pose = path[motioncounter[0]]
        robot.movearmfk(pose, armname)
        rbtmnp[0] = robotmesh.genmnp(robot)
        # bcndict = robotball.genfullactivebcndict(robot)
        # rbtmnp[1] = robotball.showcn(bcndict)
        rbtmnp[0].reparentTo(base.render)
        motioncounter[0] += 1
    else:
        motioncounter[0] = 0
    return task.again

rbtmnp = [None, None]
motioncounter = [0]
taskMgr.doMethodLater(0.05, update, "update",
                      extraArgs=[rbtmnp, motioncounter, robot, path, armname, robotmesh, robotball],
                      appendTask=True)
base.run()
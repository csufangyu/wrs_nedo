import pandaplotutils.pandactrl as pc
import utiltools.robotmath as rm

if __name__=="__main__":
    # base = pc.World(camp=[1500, 0, 1500], lookatp=[0, 0, 0])
    #
    # base.pggen.plotAxis(base.render)
    #
    # rotmat = rm.rodrigues([0,0,1], 30)
    # base.pggen.plotAxis(base.render, pandamat3 = base.pg.npToMat3(rotmat), rgba=[0.5,0.5,0.5,1])
    #
    # base.run()


    import robotsim.ur3dual.ur3dual as robot
    import robotsim.ur3dual.ur3dualmesh as robotmesh
    import manipulation.grip.robotiq85.rtq85nm as rtq85nm
    # rbt = robot.Ur3DualRobot()
    # rgthnd = rtq85nm.newHand(hndid="rgt")
    # lfthnd = rtq85nm.newHand(hndid="lft")
    # # rbtmg.gensnp(rbt, toggleendcoord=True, togglejntscoord=True).reparentTo(base.render)
    # rbtmg = robotmesh.Ur3DualMesh(rgthand=rgthnd, lfthand=lfthnd)
    # rbtmg.genmnp(rbt).reparentTo(base.render)
    # base.run()

    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    import numpy as np
    N=100
    x1 = np.linspace(0, 100, N)
    x2 = np.linspace(0.1, 1.56, N)
    x3 = np.linspace(50,150, N)
    x4 = np.linspace(10,50,N)
    dsum, rtool = np.meshgrid(x3, x4)
    y = 1.56
    # for dsum in range(100,301,100):
    uf=0.9*3*np.cos(y)/2+2*3.5*45/(rtool*0.707)
    dcom = 10*np.sqrt((4*uf*uf-9)/(9*np.sin(y)*np.sin(y)*uf*uf))
    a = 1+(dsum-dcom)*(dsum-dcom)*(np.sin(y)*np.sin(y))/100
    b = 3-3*(dsum-dcom)*dcom*(np.sin(y)*np.sin(y))/100
    c = 9/4+9*dcom*dcom*(np.sin(y)*np.sin(y))/400-72*72
    z = (-b+np.sqrt(b*b-4*a*c))/a
    # surf = ax.plot_surface(dsum, rtool, z, linewidth=1)

    rtool=30
    dsum, y = np.meshgrid(x3, x2)
    # for dsum in range(100,301,100):
    uf=0.9*3*np.cos(y)/2+2*3.5*45/(rtool*0.707)
    dcom = 10*np.sqrt((4*uf*uf-9)/(9*np.sin(y)*np.sin(y)*uf*uf))
    a = 1+(dsum-dcom)*(dsum-dcom)*(np.sin(y)*np.sin(y))/100
    b = 3-3*(dsum-dcom)*dcom*(np.sin(y)*np.sin(y))/100
    c = 9/4+9*dcom*dcom*(np.sin(y)*np.sin(y))/400-72*72
    z = (-b+np.sqrt(b*b-4*a*c))/a
    surf = ax.plot_surface(dsum, y, z, linewidth=1)

    print(z)
    # ax.set_title("Surface Plot")
    plt.show()
import cv2
import yaml
from cv2 import aruco
import utiltools.robotmath as rm
import numpy as np

if __name__=="__main__":
    mtx0, dist0, rvecs0, tvecs0, candfiles0 = yaml.load(open('../vision/camcalib/cam0_calib.yaml', 'r'))

    arucomarkersize = 209
    aruco_dict = aruco.Dictionary_get(aruco.DICT_4X4_250)
    parameters = aruco.DetectorParameters_create()

    poslist = []
    rotlist = []
    cap = cv2.VideoCapture(0)
    for i in range(100):
        print(i, 100)
        ret, img = cap.read()

        corners, ids, rejectedImgPoints = aruco.detectMarkers(img, aruco_dict, parameters=parameters)
        if ids is not None:
            aruco.drawDetectedMarkers(img, corners, borderColor=[255,255,0])
            rvecs, tvecs, _objPoints = aruco.estimatePoseSingleMarkers(corners, arucomarkersize, mtx0, dist0)
            aruco.drawAxis(img, mtx0, dist0, rvecs[0], tvecs[0]/1000.0, 0.1)
            rot = cv2.Rodrigues(rvecs[0])[0]
            pos = tvecs[0][0].ravel()
            rotlist.append(rot)
            poslist.append(pos)
    posavg = rm.posvec_average(poslist)
    rotavg = rm.rotmat_average(rotlist)
    # rotate z with 180
    rotavg = np.dot(rm.rodrigues(rotavg[:,2],180), rotavg)
    np4 = np.eye(4)
    np4[:3,:3] = rotavg
    np4[:3,3] = posavg

    savename = "cam0zero.yaml"
    with open(savename, "w") as f:
        yaml.dump(np4, f)
    cv2.destroyAllWindows()
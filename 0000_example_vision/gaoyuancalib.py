import vision.camcalib.calibrate as ccb
import os
import yaml

if __name__=="__main__":

    _this_dir, _ = os.path.split(__file__)

    gppath = os.path.join(_this_dir, "gaoyuancalib/gp/")
    kntpath = os.path.join(_this_dir, "gaoyuancalib/knt/")

    # ccb.calibcharucoboard(4,6, squaremarkersize=32, imgspath=gppath, imgformat='jpg', savename="gp.yaml")
    # ccb.calibcharucoboard(4,6, squaremarkersize=32, imgspath=kntpath, imgformat='jpg', savename="knt.yaml")
    #
    # ccb.find_rhomo(basecamyamlpath = 'knt.yaml', relcamyamlpath = 'gp.yaml', savename = 'homo_rkg.yaml')

    relmat = yaml.load(open('homo_rkg.yaml', 'r'), Loader=yaml.UnsafeLoader)
    print(relmat)
import grpc
import time
import yaml
from concurrent import futures
import rpc.plan_pb2 as pmsg
import rpc.plan_pb2_grpc as prpc

class PlanClient(object):

    def __init__(self, host = "localhost:18300"):
        channel = grpc.insecure_channel(host)
        self.stub = prpc.PlanStub(channel)
        self.__oldyaml = True
        if int(yaml.__version__[0]) >= 5:
            self.__oldyaml = False

    def sendgoals(self, poselist):
        """

        :param poselist: [[pos, rot], ...]
        :return:

        author: weiwei
        date: 20190429
        """

        self.stub.setgoals(pmsg.Goal(data = yaml.dump(poselist)))

if __name__ == "__main__":
    import numpy as np

    pclt = PlanClient(host = "localhost:18300")
    pclt.setgoals(poselist=[[np.zeros(3), np.eye(3)]])
import cv2
import os
import trimesh
import trimesh.sample as sample
import math
import yaml
import numpy as np
from cv2 import aruco
from sklearn import cluster
import utiltools.robotmath as rm
from pandaplotutils import pandactrl
from vision.camcalib import arucotrack_fusion as af
import motionplanning.collisioncheckerball as cdball
import pickle
from environment import collisionmodel as cm
from panda3d.core import *
from direct.gui.DirectGui import *
import copy



def dispcamera(camcaps, cammtxs, camdists, camrelhomos,frameavglist):

    for x in range(len(camcaps)):
        frame1 = camcaps[x].read()[1]

        if frame1 is not None:
            if x == 0:
                for id in frameavglist:
                    posvecavg = frameavglist[id][0]
                    rotmatavg,_ = cv2.Rodrigues(frameavglist[id][1])
                    aruco.drawAxis(frame1, cammtxs[x], camdists[x], rotmatavg, posvecavg/1000.0, 0.1)

                cv2.imshow('frame' + str(x), frame1)
                cv2.moveWindow('frame'+str(x), 450, 200)
            elif x == 1:
                for id in frameavglist:
                    posvecavg = frameavglist[id][0]
                    matinb = np.dot(camrelhomos[x-1], rm.homobuild(posvecavg, frameavglist[id][1]))
                    rotmatavg,_ = cv2.Rodrigues(matinb[:3,:3])
                    posvecavg = matinb[:3,3]
                    aruco.drawAxis(frame1, cammtxs[x], camdists[x], rotmatavg, posvecavg/1000.0, 0.1)

                cv2.imshow('frame' + str(x), frame1)
                cv2.moveWindow('frame'+str(x), 100, 800)
            elif x == 2:
                for id in frameavglist:
                    posvecavg = frameavglist[id][0]
                    matinb = np.dot(camrelhomos[x-1], rm.homobuild(posvecavg, frameavglist[id][1]))
                    rotmatavg,_ = cv2.Rodrigues(matinb[:3,:3])
                    posvecavg = matinb[:3,3]
                    aruco.drawAxis(frame1, cammtxs[x], camdists[x], rotmatavg, posvecavg/1000.0, 0.1)

                cv2.imshow('frame' + str(x), frame1)
                cv2.moveWindow('frame'+str(x), 800, 800)

        k = cv2.waitKey(1)
        if cv2.waitKey(1) == 27:
            exit()

def loadobj(name):
    this_dir, this_filename = os.path.split(__file__)
    objpath = os.path.join(this_dir, "objects", name)
    objtrimesh = trimesh.load_mesh(objpath)
    # assume 40mm diameter
    evensamples = sample.sample_surface_even(objtrimesh, int(math.ceil(objtrimesh.area/200.0)))
    objnp = base.pg.loadstlaspandanp_fn(objpath)
    cdobjnp = objnp.attachNewNode(cdball.getcdboxcn(objnp))
    cdobjnp.setTag('boxcd', 'true')
    objnp.setTag('samples', pickle.dumps(evensamples))
    return [objnp, cdobjnp, objpath]

def dispobject(base, objnp, posvecavg, rotmatavg):

    mtx = np.eye(4)
    mtx[:3,:3] = rotmatavg
    mtx[:3,3] = posvecavg
    print(mtx)
    obsrotmat1 = base.pg.np4ToMat4(mtx)
    objnp.setMat(obsrotmat1)
    objnp.setColor(.3, .3, .6, 1)
    objnp.reparentTo(base.render)


def dispcharucoboard(camcaps, cammtxs, camdists, nrow, ncolumn, arucomarkerdict=aruco.DICT_6X6_250, squaremarkersize=25,):
    """

    :param nrow:
    :param ncolumn:
    :param markerdict:
    :param imgspath:
    :param savename:
    :return:

    author: weiwei
    date: 20190420
    """

    # read images and detect cornders
    aruco_dict = aruco.Dictionary_get(arucomarkerdict)
    allCorners = []
    allIds = []
    # SUB PIXEL CORNER DETECTION CRITERION
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 27, 0.0001)

    board = aruco.CharucoBoard_create(ncolumn, nrow, squaremarkersize, .57*squaremarkersize, aruco_dict)
    arucoParams = aruco.DetectorParameters_create()

    ret, frame = camcaps.read()
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    corners, ids, rejectedImgPoints = cv2.aruco.detectMarkers(gray, aruco_dict, parameters=arucoParams)
    aruco.refineDetectedMarkers(frame, board, corners, ids, rejectedImgPoints)
    if len(corners) > 0:
        # SUB PIXEL DETECTION
        for corner in corners:
            cv2.cornerSubPix(gray, corner, winSize=(2, 2), zeroZone=(-1, -1), criteria=criteria)
        res2 = cv2.aruco.interpolateCornersCharuco(corners, ids, gray, board)

        charucoretval, charucoCorners, charucoIds = aruco.interpolateCornersCharuco(corners, ids, gray, board)
        retval, rvec, tvec = aruco.estimatePoseCharucoBoard(charucoCorners, charucoIds, board, cammtxs, camdists)
        if retval is True:
            ############何故かできない！！！############
            aruco.drawAxis(frame, cammtxs, camdists, rvec, tvec, 0.1)  # Draw Axis

        # require len(res2[1]) > nrow*ncolumn/2 at least half of the corners are detected
        if res2[1] is not None and res2[2] is not None and len(res2[1]) > (nrow-1)*(ncolumn-1)/2:
            allCorners.append(res2[1])
            allIds.append(res2[2])
        imaxis = aruco.drawDetectedMarkers(frame, corners, ids)
        imaxis = aruco.drawDetectedCornersCharuco(imaxis, res2[1], res2[2], (255,255,0))


        cv2.imshow('frame', imaxis)
        cv2.waitKey(100)

    # The calibratedCameraCharucoExtended function additionally estimate calibration errors
    # Thus, stdDeviationsIntrinsics, stdDeviationsExtrinsics, perViewErrors are returned
    # We dont use them here though
    # see https://docs.opencv.org/3.4.6/d9/d6a/group__aruco.html for details

if __name__=='__main__':



    # squaremarkersize = 40
    #
    # calibcharucoboard(7,5, squaremarkersize=squaremarkersize, imgspath='./camimgs0/', savename='cam0_calib.yaml')
    # calibcharucoboard(7,5, squaremarkersize=squaremarkersize, imgspath='./camimgs2/', savename='cam2_calib.yaml')
    # calibcharucoboard(7,5, squaremarkersize=squaremarkersize, imgspath='./camimgs4/', savename='cam4_calib.yaml')

    # find_rhomo(basecamyamlpath = 'cam0_calib.yaml', relcamyamlpath = 'cam2_calib.yaml', savename = 'homo_rb20.yaml')
    # find_rhomo(basecamyamlpath = 'cam0_calib.yaml', relcamyamlpath = 'cam4_calib.yaml', savename = 'homo_rb40.yaml')

    base = pandactrl.World(camp=[500, 500, -2000], lookatp=[0, 0, 100], up=[0,0,-1], fov = 40, w = 1920, h = 1080)
    # framenp = base.pggen.genAxis()
    # framenp.reparentTo(base.render)
    # base.run()

    base.pggen.plotAxis(base.render)
    homo_rb20 = yaml.load(open('../vision/camcalib/homo_rb20.yaml', 'r'))
    homo_rb40 = yaml.load(open('../vision/camcalib/homo_rb40.yaml', 'r'))

    # draw in 3d to validate
    pandamat4homo_r2 = base.pg.np4ToMat4(rm.homoinverse(homo_rb20))
    base.pggen.plotAxis(base.render, spos = pandamat4homo_r2.getRow3(3), pandamat3 = pandamat4homo_r2.getUpper3())

    pandamat4homo_r4 = base.pg.np4ToMat4(rm.homoinverse(homo_rb40))
    base.pggen.plotAxis(base.render, spos = pandamat4homo_r4.getRow3(3), pandamat3 = pandamat4homo_r4.getUpper3())

    # show in videos
    mtx0, dist0, rvecs0, tvecs0, candfiles0 = yaml.load(open('../vision/camcalib/cam0_calib.yaml', 'r'))

    import time
    # arucomarkersize = int(40*.57)
    # aruco_dict = aruco.Dictionary_get(aruco.DICT_6X6_250)

    cap0 = cv2.VideoCapture(0)

    camcaps = cap0
    cammtxs = mtx0
    camdists = dist0
    aruco_dict = aruco.Dictionary_get(aruco.DICT_6X6_250)
    arucomarkersize = 100
    nframe = 2
    denoise = True
    framenplist = [[]]

    def updateview(framenplist, task):

        if len(framenplist[0]) > 0:
            for axisnp in framenplist[0]:
                axisnp.removeNode()
            framenplist[0] = []
        tic = time.time()
        frameavglist = dispcharucoboard(camcaps, cammtxs, camdists, 3, 2, arucomarkerdict=aruco.DICT_7X7_250, squaremarkersize=90,)
        # print(time.time()-tic)
        # for id in frameavglist:
        #     posvecavg = frameavglist[id][0]
        #     rotmatavg = frameavglist[id][1]
        #     framenp = base.pggen.genAxis(spos=base.pg.npToV3(posvecavg), pandamat3=base.pg.npToMat3(rotmatavg))
        #     framenp.reparentTo(base.render)
        #     framenplist[0].append(framenp)



        return task.again


    taskMgr.doMethodLater(0.01, updateview, "updateview", extraArgs=[framenplist], appendTask=True)

    base.run()
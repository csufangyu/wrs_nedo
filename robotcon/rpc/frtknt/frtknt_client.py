import grpc
import numpy as np
import robotcon.rpc.frtknt.frtknt_pb2 as fkmsg
import robotcon.rpc.frtknt.frtknt_pb2_grpc as fkrpc

class FrtKnt(object):

    def __init__(self, host = "localhost:18300"):
        channel = grpc.insecure_channel(host)
        self.stub = fkrpc.KntStub(channel)
        self.__oldyaml = True
        if int(yaml.__version__[0]) >= 5:
            self.__oldyaml = False

    def __unpackarraydata(self, dobj):
        h = dobj.width
        w = dobj.height
        ch = dobj.channel
        return np.array.frombuffer(dobj.image, (h,w,ch))

    def getrgbimg(self):
        """
        get color image as an array

        :return: a colorHeight*colorWidth*4 np array, the second and third channels are repeated
        author: weiwei
        date: 20180207
        """

        rgbimg = self.stub.getrgbimg(fkmsg.Empty())
        return self.__unpackarraydata(rgbimg)

    def getdepthimg(self):
        """
        get depth image as an array

        :return: a depthHeight*depthWidth*3 np array, the second and third channels are repeated
        author: weiwei
        date: 20180207
        """

        depthimg = self.stub.getdepthimg(fkmsg.Empty())
        return self.__unpackarraydata(depthimg)

    def getpcd(self):
        """
        get the full poind cloud of a new frame as an array

        :param mat_kw 4x4 nparray
        :return: np.array point cloud n-by-3
        author: weiwei
        date: 20181121
        """

        pcd = self.stub.getpcd(fkmsg.Empty())
        return np.ndarray.frombuffer(pcd)

    def getpartialpcd(self, dframe, width, height):
        """
        get partial poind cloud using the given picklerawdframe, width, height in a depth img

        :param dframe return value of getdepthraw
        :param width, height
        :param picklemat_tw pickle string storing mat_tw

        author: weiwei
        date: 20181121
        """

        widthpairmsg = fkmsg.Pair(data0 = width[0], data1 = width[1])
        heightpairmsg = fkmsg.Pair(data0 = height[0], data1 = height[1])
        h, w, ch = dframe.shape
        dframedata = fkmsg.CamImg(width=w, height=h, channel=ch, image=np.ndarray.tobytes(dframe))
        dframemsg = fkmsg.PartialPcdPara(data = dframedata, width = widthpairmsg,
                                         height = heightpairmsg)
        dobj = self.stub.getpartialpcd(dframemsg)
        return np.ndarray.frombuffer(dobj)

    def mapColorPointToCameraSpace(self, pt):
        """
        convert color space  , to depth space point

        :param pt: [p0, p1] or nparray([p0, p1])
        :return:
        author: weiwei
        date: 20181121
        """

        ptpairmsg = fkmsg.Pair(data0 = pt[0], data1 = pt[1])
        dobj = self.stub.mapColorPointToCameraSpace(pt)
        return np.ndarray.frombuffer(dobj)

if __name__ == "__main__":
    import robotcon.rpc.frtknt.frtknt_client as fc

    frk = fc.FrtKnt(host = "localhost:18300")
    pcd = frk.getpcdarray()
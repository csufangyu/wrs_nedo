import os
import time
import numpy as np
from panda3d.core import Mat4
import pandaplotutils.pandactrl as pandactrl
import pandaplotutils.pandageom as pandageom
import environment.collisionmodel as cm
from manipulation.grip import freegripcontactpairs as fgcp
from database import dbcvt as dc
import environment.bulletcdhelper as bcd
import utiltools.robotmath as rm
import trimesh.sample as sample
from sklearn.neighbors import RadiusNeighborsClassifier
import environment.bulletcdhelper as bh

class FreegripRS(object):
    """
    free grip using ray shooting
    """

    def __init__(self, objcm):
        """

        :param objpath:
        """

        self.objcm = objcm
        self.objtrimesh = objcm.trimesh
        self.samples = None
        self.samples_face_idx = None
        self.samplesnrmals = None
        self.samplepnts_refcls = None
        self.samplenrmls_refcls = None
        self.mc = bh.MCMchecker()
        self.__sampleObjModel()

        self.contactpairs = None
        self.gripcontacts_planned = None
        self.griprotmats_planned = None
        self.gripjawwidth_planned = None
        self.gripcontactnormals_planned = None

    def __sampleObjModel(self, numpointsoververts=5, reduceRadius=10):
        """
        sample the object model
        self.objsamplepnts and self.objsamplenrmls
        are filled in this function

        :param: numpointsoververts: the number of sampled points = numpointsoververts*mesh.vertices.shape[0]
        :return: nverts: the number of verts sampled

        author: weiwei
        date: 20160623 flight to tokyo
        """

        nverts = self.objtrimesh.vertices.shape[0]
        tic = time.time()
        self.samples, self.samples_face_idx = sample.sample_surface_even_withfaceid(self.objtrimesh,
                                                       count=(1000 if nverts*numpointsoververts > 1000 \
                                                                  else nverts*numpointsoververts))
        toc = time.time()
        print("sampling cost", toc-tic)
        self.samplenrmals = self.objtrimesh.face_normals[self.samples_face_idx]

        # remove bad samples x
        # clusterrnn
        tic = time.time()
        neigh = RadiusNeighborsClassifier(radius=1.0)
        X = self.samples
        nX = X.shape[0]
        neigh.fit(X, range(nX))
        neigharrays = neigh.radius_neighbors(X, radius=reduceRadius, return_distance=False)
        self.samplepnts_refcls = []
        self.samplenrmls_refcls = []
        delset = set([])
        for j in range(nX):
            if j not in delset:
                self.samplepnts_refcls.append(np.array(X[j]))
                self.samplenrmls_refcls.append(np.array(self.samplenrmals[j]))
                delset.update(neigharrays[j].tolist())
        toc = time.time()
        print("cluster samples cost", toc-tic)

    def __planContactPairs(self, pairopposite = -.9):
        """
        find the contact pairs using rayshooting

        :return:

        author: weiwei
        date: 20190805
        """

        self.contactpairs = []

        for i, cpt0 in enumerate(self.samplepnts_refcls):
            hitlist = self.mc.isRayHitMeshAll(cpt0-self.samplenrmls_refcls[i]*.1, cpt0-self.samplenrmls_refcls[i]*10000, self.objcm)
            if len(hitlist) > 0:
                for cpt1, cnrmal1 in hitlist:
                    if np.dot(self.samplenrmls_refcls[i], cnrmal1)<pairopposite:
                        self.contactpairs.append(([cpt0, self.samplenrmls_refcls[i]], [cpt1, cnrmal1]))

    def planGrasps(self, hand, pairopposite=-.9, discretesize=8, contactoffset=7):
        """
        find the collision free grasps

        :param: hand: see the definition in manipulation.grip.xxx
        :return:
        """

        self.__planContactPairs(pairopposite)

        self.gripcontacts_planned = []
        self.griprotmats_planned = []
        self.gripjawwidth_planned = []
        self.gripcontactnormals_planned = []

        for contactpair in self.contactpairs:
            cpt0 = contactpair[0][0]
            nrmal0 = contactpair[0][1]
            cpt1 = contactpair[1][0]
            nrmal1 = contactpair[1][1]
            fingerdistance = np.linalg.norm(cpt0-cpt1)+contactoffset
            if fingerdistance < hand.jawwidthopen:
                # pose, rotate, and test collision
                fingercenter = (cpt0 + cpt1)/2.0
                avgcontactnormal = rm.unitvec_safe(nrmal0-nrmal1)[1]
                for angleid in range(discretesize):
                    rotangle = 360.0 / discretesize * angleid
                    hand.gripAt(fingercenter[0], fingercenter[1], fingercenter[2],
                                     avgcontactnormal[0], avgcontactnormal[1], avgcontactnormal[2],
                                     rotangle, jawwidth = fingerdistance)
                    result = self.mc.isMeshMeshListCollided(self.objcm, hand.cmlist)
                    if not result:
                        # contact0 first
                        self.gripcontacts_planned.append(contactpair)
                        pdrotmat4 = Mat4(hand.getMat())
                        self.griprotmats_planned.append(pdrotmat4)
                        self.gripjawwidth_planned.append(fingerdistance)
                        self.gripcontactnormals_planned.append([avgcontactnormal, -avgcontactnormal])
                        # flip
                        self.gripcontacts_planned.append([contactpair[1], contactpair[0]])
                        # rotate 180 around local z
                        pdrotmat4flip = Mat4(pdrotmat4)
                        pdrotmat4flip.setRow(0, -pdrotmat4.getRow3(0))
                        pdrotmat4flip.setRow(1, -pdrotmat4.getRow3(1))
                        self.griprotmats_planned.append(pdrotmat4flip)
                        self.gripjawwidth_planned.append(fingerdistance)
                        self.gripcontactnormals_planned.append([-avgcontactnormal, avgcontactnormal])


if __name__=='__main__':
    import manipulation.grip.robotiq85.robotiq85 as hnd
    import environment.collisionmodel as cm
    from panda3d.core import GeomNode
    from panda3d.core import NodePath
    from panda3d.core import Vec4

    # base = pandactrl.World(camp=[700,300,700], lookatp=[0,0,100])
    base = pandactrl.World(camp=[700,-300,300], lookatp=[0,0,100])

    objpath = "../objects/sandpart.stl"
    # objpath = "../objects/ttube.stl"
    objcm = cm.CollisionModel(objinit = objpath)
    objcm.setColor(.7, .7, .7, 1)
    objcm.reparentTo(base.render)

    hndfa = hnd.Robotiq85Factory()
    hand = hndfa.genHand()
    freegriprstst = FreegripRS(objcm)

    tic = time.time()
    freegriprstst.planGrasps(hand)
    toc = time.time()
    print("plan grasp cost", toc-tic)

    print("number of grasps planned", len(freegriprstst.griprotmats_planned))
    print("number of samples", len(freegriprstst.samplepnts_refcls))
    print("number of contact pairs", len(freegriprstst.contactpairs))
    for pnt in freegriprstst.samplepnts_refcls:
        base.pggen.plotSphere(base.render, pos=pnt, radius=4, rgba=[.7,0,0,1])

    def update(freegriprstst, task):
        if base.inputmgr.keyMap['space'] is True:
            for contactpair in freegriprstst.contactpairs:
                cpt0 = contactpair[0][0]
                nrmal0 = contactpair[0][1]
                cpt1 = contactpair[1][0]
                nrmal1 = contactpair[1][1]
                base.pggen.plotSphere(base.render, pos=cpt0, radius=5, rgba=[.7,0,0,1])
                base.pggen.plotArrow(base.render, spos=cpt0, epos=cpt0+nrmal0, length=10, thickness=2, rgba=[.7,0,0,1])
                base.pggen.plotSphere(base.render, pos=cpt1, radius=5, rgba=[0,0,.7,1])
                base.pggen.plotArrow(base.render, spos=cpt1, epos=cpt1+nrmal1, length=10, thickness=2, rgba=[0,0,.7,1])
            base.inputmgr.keyMap['space'] = False
        if base.inputmgr.keyMap['g'] is True:
            for i, freegriprotmat in enumerate(freegriprstst.griprotmats_planned):
                hand = hndfa.genHand()
                hand.setColor(1,1,1,.3)
                newpos = freegriprotmat.getRow3(3)-freegriprotmat.getRow3(2)*0.0
                freegriprotmat.setRow(3, newpos)
                hand.setMat(pandamat4=freegriprotmat)
                hand.setJawwidth(freegriprstst.gripjawwidth_planned[i])
                hand.reparentTo(base.render)
            base.inputmgr.keyMap['g'] = False
        return task.again

    taskMgr.doMethodLater(0.05, update, "update",
                          extraArgs=[freegriprstst],
                          appendTask=True)
    base.run()
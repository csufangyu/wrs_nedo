import grpc
import time
import yaml
from concurrent import futures
import manipulation.pickandplace.rpc.plan_pb2 as pmsg
import manipulation.pickandplace.rpc.plan_pb2_grpc as prpc

class PlanClient(object):

    def __init__(self, host = "localhost:18300"):
        channel = grpc.insecure_channel(host)
        self.stub = prpc.PlanStub(channel)
        self.__oldyaml = True
        if int(yaml.__version__[0]) >= 5:
            self.__oldyaml = False

    def sendgoals(self, poselist):
        """

        :param poselist: [[pos, rot], ...]
        :return:

        author: weiwei
        date: 20190429
        """

        self.stub.setgoals(pmsg.Goal(data = yaml.dump(poselist)))

if __name__ == "__main__":
    import numpy as np

    import robotsim.ur3dual.ur3dual as robot
    import robotsim.ur3dual.ur3dualmesh as robotmesh
    import robotsim.ur3dual.ur3dualball as robotball
    import manipulation.grip.robotiq85.robotiq85 as rtq85
    from pandaplotutils import pandactrl as pandactrl
    import bunrisettingfree as bsf
    import utiltools.robotmath as rm
    import random

    base = pandactrl.World(camp = [5000,0,3000], lookatp = [0,0,700])
    env = bsf.Env(boundingradius=15)
    env.reparentTo(base.render)

    hndfa = rtq85.Robotiq85Factory()
    rgthnd = hndfa.genHand()
    lfthnd = hndfa.genHand()
    rbt = robot.Ur3DualRobot(rgthnd, lfthnd)
    rbt.goinitpose()
    rbtmg = robotmesh.Ur3DualMesh()
    rbtmg.genmnp(rbt).reparentTo(base.render)

    pclt = PlanClient(host = "localhost:18300")

    def update(pclt, counter, task):
        if base.inputmgr.keyMap['space'] is True:
            pos = [200, 150, 1040+counter[0]*10]
            rot = np.dot(rm.rodrigues([0,0,1], -110), rm.rodrigues([0,1,0], 90))
            pclt.sendgoals(poselist=[[pos, rot]])
            base.inputmgr.keyMap['space'] = False
            counter[0] += 1
        return task.cont

    counter = [1]
    taskMgr.doMethodLater(0.05, update, "update", extraArgs=[pclt, counter], appendTask=True)
    base.run()
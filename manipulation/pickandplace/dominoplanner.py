from pandaplotutils import pandactrl as pandactrl
import manipulation.grip.robotiq85.robotiq85 as rtq85
import robotsim.ur3dual.ur3dual as robot
import robotsim.ur3dual.ur3dualmesh as robotmesh
import robotsim.ur3dual.ur3dualball as robotball
import utiltools.robotmath as rm
import numpy as np
import environment.collisionmodel as cm
import bunrisettingfree as bsf
import motionplanning.collisioncheckerball as cdball
import environment.bulletcdhelper as bch
from motionplanning import collisioncheckerball as cdck
from motionplanning import ctcallback as ctcb
from motionplanning.rrt import rrtconnect as rrtc
from motionplanning import smoother as sm
import manipulation.pickandplace.sharedgrasps as sg
import copy
import manipulation.pickandplace.rpc.plan_server as server1
import grpc
import manipulation.pickandplace.rpc.plan_pb2_grpc as prpc
from concurrent import futures
import time
import math
import os

if __name__=="__main__":
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    pserv = server1.PlanServer()
    pserv.initialize()
    prpc.add_PlanServicer_to_server(pserv, server)
    server.add_insecure_port('[::]:18300')
    server.start()
    print("The Plan server is started!")

    base = pandactrl.World(camp = [5000,0,3000], lookatp = [0,0,700])
    env = bsf.Env(boundingradius=15)
    env.reparentTo(base.render)

    hndfa = rtq85.Robotiq85Factory()
    rgthnd = hndfa.genHand()
    lfthnd = hndfa.genHand()
    rbt = robot.Ur3DualRobot(rgthnd, lfthnd)
    rbt.goinitpose()
    rbtmg = robotmesh.Ur3DualMesh()
    rbtmg.genmnp(rbt).reparentTo(base.render)

    rbtball = robotball.Ur3DualBall()

    _this_dir, _ = os.path.split(__file__)
    _objpath = os.path.join(_this_dir, "objects", "domino.stl")
    obj = cm.CollisionModel(_objpath)

    isgoalobtained = [False]
    goallist = [[]]
    finishedlist = [[]]
    rotatematrix = rm.rodrigues([0,0,1],-70)
    finishpos = np.array([250,370,1045])
    distancebetweeneachother = 40
    xydominogoals = [[finishpos, rotatematrix],
                     [finishpos+distancebetweeneachother*rotatematrix[:3,0], rotatematrix],
                     [finishpos+distancebetweeneachother*2*rotatematrix[:3,0], rotatematrix],
                     [finishpos+distancebetweeneachother*3*rotatematrix[:3,0], rotatematrix],
                     [finishpos+distancebetweeneachother*4*rotatematrix[:3,0], rotatematrix]]

    goalcounter = [0]

    # import robotcon.ur3dual as ud
    # uc = ud.Ur3DualUrx()
    # uc.movejntssgl(rbt.initlftjnts, armname="lft")
    def update(pserv, isgoalobtained, goallist, obj, rtq85, rbtball, finishedlist,xydominogoals, goalcounter, task):
        if len(pserv.poselist) > 0 and not isgoalobtained[0]:
            print(pserv.poselist)
            print(goallist[0])
            for i in range(len(pserv.poselist)):
                goallist[0].append(rm.homobuild(pserv.poselist[i][0], pserv.poselist[i][1]))
                boxtransform = np.eye(4)
                boxtransform[:3, :3] = pserv.poselist[i][1]
                boxcenter = pserv.poselist[i][0]
                boxcm = copy.deepcopy(obj)
                boxcm.setMat(base.pg.npToMat4(boxtransform[:3, :3], boxcenter))

                boxcm.setColor(1, 1, 1, 1)
                boxcm.reparentTo(base.render)
            isgoalobtained[0] = True

            obscmlist = env.getstationaryobslist()

            hndfa = rtq85.Robotiq85Factory()
            rtq85 = hndfa.genHand()
            base.pggen.plotAxis(rtq85.handnp, length=15, thickness=2)
            predefined_grasps = []
            predefined_grasps.append(rtq85.approachAt(0, 0, 90, 0, 1, 0, -1, 0, -1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 90, 0, 1, 0, 1, 0, -1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 90, 0, 1, 0, -1, 0, -2, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 90, 0, 1, 0, 1, 0, -2, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 90, 0, 1, 0, -2, 0, -1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 90, 0, 1, 0, 2, 0, -1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 90, 0, 1, 0, -1, 0, 1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 90, 0, 1, 0, 1, 0, 1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 90, 0, 1, 0, -1, 0, 2, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 90, 0, 1, 0, 1, 0, 2, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 90, 0, 1, 0, -2, 0, 1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 90, 0, 1, 0, 2, 0, 1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 90, 0, 1, 0, -1, 0, 0, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 90, 0, 1, 0, 1, 0, 0, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 90, 0, 1, 0, 0, 0, -1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 30, 0, 1, 0, -1, 0, -1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 30, 0, 1, 0, 1, 0, -1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 30, 0, 1, 0, -1, 0, -2, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 30, 0, 1, 0, 1, 0, -2, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 30, 0, 1, 0, -2, 0, -1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 30, 0, 1, 0, 2, 0, -1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 30, 0, 1, 0, -1, 0, 1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 30, 0, 1, 0, 1, 0, 1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 30, 0, 1, 0, -1, 0, 2, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 30, 0, 1, 0, 1, 0, 2, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 30, 0, 1, 0, -2, 0, 1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 30, 0, 1, 0, 2, 0, 1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 30, 0, 1, 0, -1, 0, 0, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 30, 0, 1, 0, 1, 0, 0, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 30, 0, 1, 0, 0, 0, 1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 110, 0, 1, 0, -1, 0, -1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 110, 0, 1, 0, 1, 0, -1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 110, 0, 1, 0, -1, 0, -2, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 110, 0, 1, 0, 1, 0, -2, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 110, 0, 1, 0, -2, 0, -1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 110, 0, 1, 0, 2, 0, -1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 110, 0, 1, 0, -1, 0, 1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 110, 0, 1, 0, 1, 0, 1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 110, 0, 1, 0, -1, 0, 2, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 110, 0, 1, 0, 1, 0, 2, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 110, 0, 1, 0, -2, 0, 1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 110, 0, 1, 0, 2, 0, 1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 110, 0, 1, 0, -1, 0, 0, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 110, 0, 1, 0, 1, 0, 0, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 110, 0, 1, 0, 0, 0, -1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 10, 0, 1, 0, -1, 0, -1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 10, 0, 1, 0, 1, 0, -1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 10, 0, 1, 0, -1, 0, -2, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 10, 0, 1, 0, 1, 0, -2, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 10, 0, 1, 0, -2, 0, -1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 10, 0, 1, 0, 2, 0, -1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 10, 0, 1, 0, -1, 0, 1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 10, 0, 1, 0, 1, 0, 1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 10, 0, 1, 0, -1, 0, 2, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 10, 0, 1, 0, 1, 0, 2, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 10, 0, 1, 0, -2, 0, 1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 10, 0, 1, 0, 2, 0, 1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 10, 0, 1, 0, -1, 0, 0, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 10, 0, 1, 0, 1, 0, 0, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 10, 0, 1, 0, 0, 0, 1, jawwidth=60))

            predefined_grasps.append(rtq85.approachAt(0, 0, 90, 0, -1, 0, -1, 0, -1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 90, 0, -1, 0, 1, 0, -1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 90, 0, -1, 0, -1, 0, -2, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 90, 0, -1, 0, 1, 0, -2, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 90, 0, -1, 0, -2, 0, -1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 90, 0, -1, 0, 2, 0, -1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 90, 0, -1, 0, -1, 0, 1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 90, 0, -1, 0, 1, 0, 1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 90, 0, -1, 0, -1, 0, 2, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 90, 0, -1, 0, 1, 0, 2, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 90, 0, -1, 0, -2, 0, 1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 90, 0, -1, 0, 2, 0, 1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 90, 0, -1, 0, -1, 0, 0, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 90, 0, -1, 0, 1, 0, 0, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 90, 0, -1, 0, 0, 0, -1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 30, 0, -1, 0, -1, 0, -1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 30, 0, -1, 0, 1, 0, -1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 30, 0, -1, 0, -1, 0, -2, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 30, 0, -1, 0, 1, 0, -2, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 30, 0, -1, 0, -2, 0, -1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 30, 0, -1, 0, 2, 0, -1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 30, 0, -1, 0, -1, 0, 1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 30, 0, -1, 0, 1, 0, 1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 30, 0, -1, 0, -1, 0, 2, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 30, 0, -1, 0, 1, 0, 2, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 30, 0, -1, 0, -2, 0, 1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 30, 0, -1, 0, 2, 0, 1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 30, 0, -1, 0, -1, 0, 0, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 30, 0, -1, 0, 1, 0, 0, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 30, 0, -1, 0, 0, 0, 1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 110, 0, -1, 0, -1, 0, -1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 110, 0, -1, 0, 1, 0, -1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 110, 0, -1, 0, -1, 0, -2, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 110, 0, -1, 0, 1, 0, -2, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 110, 0, -1, 0, -2, 0, -1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 110, 0, -1, 0, 2, 0, -1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 110, 0, -1, 0, -1, 0, 1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 110, 0, -1, 0, 1, 0, 1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 110, 0, -1, 0, -1, 0, 2, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 110, 0, -1, 0, 1, 0, 2, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 110, 0, -1, 0, -2, 0, 1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 110, 0, -1, 0, 2, 0, 1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 110, 0, -1, 0, -1, 0, 0, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 110, 0, -1, 0, 1, 0, 0, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 110, 0, -1, 0, 0, 0, -1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 10, 0, -1, 0, -1, 0, -1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 10, 0, -1, 0, 1, 0, -1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 10, 0, -1, 0, -1, 0, -2, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 10, 0, -1, 0, 1, 0, -2, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 10, 0, -1, 0, -2, 0, -1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 10, 0, -1, 0, 2, 0, -1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 10, 0, -1, 0, -1, 0, 1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 10, 0, -1, 0, 1, 0, 1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 10, 0, -1, 0, -1, 0, 2, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 10, 0, -1, 0, 1, 0, 2, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 10, 0, -1, 0, -2, 0, 1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 10, 0, -1, 0, 2, 0, 1, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 10, 0, -1, 0, -1, 0, 0, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 10, 0, -1, 0, 1, 0, 0, jawwidth=60))
            predefined_grasps.append(rtq85.approachAt(0, 0, 10, 0, -1, 0, 0, 0, 1, jawwidth=60))

            # predefined_grasps.append(rtq85.approachAt(0, -20, 60, -1, 0, 0, 0, 1, 0, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 60, -1, 0, 0, 0, 1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 60, -1, 0, 0, 0, 1, -1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 60, -1, 0, 0, 0, -1, -1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 60, -1, 0, 0, 0, -1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 60, -1, 0, 0, 0, -1, 0, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 10, -1, 0, 0, 0, -1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 10, -1, 0, 0, 0, 1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 10, -1, 0, 0, 0, -1, 0, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 10, -1, 0, 0, 0, 1, 0, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 110, -1, 0, 0, 0, -1, -1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 110, -1, 0, 0, 0, 1, -1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 110, -1, 0, 0, 0, -1, 0, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 110, -1, 0, 0, 0, 1, 0, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 30, -1, 0, 0, 0, -1, -1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 30, -1, 0, 0, 0, -1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 30, -1, 0, 0, 0, 1, -1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 30, -1, 0, 0, 0, 1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 30, -1, 0, 0, 0, 1, 0, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 90, -1, 0, 0, 0, -1, -1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 90, -1, 0, 0, 0, -1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 90, -1, 0, 0, 0, -1, 0, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 90, -1, 0, 0, 0, 1, -1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 90, -1, 0, 0, 0, 1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 90, -1, 0, 0, 0, 1, 0, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 110, -1, 0, 0, 0, 0, -1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 110, -1, 0, 0, 0, 0, -1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 0, 110, -1, 0, 0, 0, 0, -1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 0, 110, -1, 0, 0, 0, 1, -1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 0, 110, -1, 0, 0, 0, -1, -1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 10, -1, 0, 0, 0, 0, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 10, -1, 0, 0, 0, 0, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 0, 10, -1, 0, 0, 0, 0, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 0, 10, -1, 0, 0, 0, 1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 0, 10, -1, 0, 0, 0, -1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 105, -1, 0, 0, 0, -1, -1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 100, -1, 0, 0, 0, -1, -1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 95, -1, 0, 0, 0, -1, -1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 90, -1, 0, 0, 0, -1, -1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 85, -1, 0, 0, 0, -1, -1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 105, -1, 0, 0, 0, -1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 100, -1, 0, 0, 0, -1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 95, -1, 0, 0, 0, -1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 90, -1, 0, 0, 0, -1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 85, -1, 0, 0, 0, -1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 80, -1, 0, 0, 0, -1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 75, -1, 0, 0, 0, -1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 70, -1, 0, 0, 0, -1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 65, -1, 0, 0, 0, -1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 60, -1, 0, 0, 0, -1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 55, -1, 0, 0, 0, -1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 50, -1, 0, 0, 0, -1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 45, -1, 0, 0, 0, -1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 40, -1, 0, 0, 0, -1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 35, -1, 0, 0, 0, -1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 50, -1, 0, 0, 0, 1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 60, -1, 0, 0, 0, 1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 70, -1, 0, 0, 0, 1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 80, -1, 0, 0, 0, 1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 90, -1, 0, 0, 0, 1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 100, -1, 0, 0, 0, 1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 95, -1, 0, 0, 0, 1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 85, -1, 0, 0, 0, 1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 75, -1, 0, 0, 0, 1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 50, -1, 0, 0, 0, 1, -1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 55, -1, 0, 0, 0, 1, -1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 60, -1, 0, 0, 0, 1, -1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 65, -1, 0, 0, 0, 1, -1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 70, -1, 0, 0, 0, 1, -1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 75, -1, 0, 0, 0, 1, -1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 80, -1, 0, 0, 0, 1, -1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 85, -1, 0, 0, 0, 1, -1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 90, -1, 0, 0, 0, 1, -1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 95, -1, 0, 0, 0, 1, -1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 100, -1, 0, 0, 0, 1, -1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 45, -1, 0, 0, 0, 1, -1, jawwidth=20))
            #
            # predefined_grasps.append(rtq85.approachAt(0, -20, 60, 1, 0, 0, 0, 1, 0, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 60, 1, 0, 0, 0, 1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 60, 1, 0, 0, 0, 1, -1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 60, 1, 0, 0, 0, -1, -1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 60, 1, 0, 0, 0, -1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 60, 1, 0, 0, 0, -1, 0, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 10, 1, 0, 0, 0, -1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 10, 1, 0, 0, 0, 1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 10, 1, 0, 0, 0, -1, 0, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 10, 1, 0, 0, 0, 1, 0, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 110, 1, 0, 0, 0, -1, -1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 110, 1, 0, 0, 0, 1, -1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 110, 1, 0, 0, 0, -1, 0, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 110, 1, 0, 0, 0, 1, 0, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 30, 1, 0, 0, 0, -1, -1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 30, 1, 0, 0, 0, -1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 30, 1, 0, 0, 0, 1, -1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 30, 1, 0, 0, 0, 1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 30, 1, 0, 0, 0, 1, 0, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 90, 1, 0, 0, 0, -1, -1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 90, 1, 0, 0, 0, -1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 90, 1, 0, 0, 0, -1, 0, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 90, 1, 0, 0, 0, 1, -1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 90, 1, 0, 0, 0, 1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 90, 1, 0, 0, 0, 1, 0, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 110, 1, 0, 0, 0, 0, -1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 110, 1, 0, 0, 0, 0, -1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 0, 110, 1, 0, 0, 0, 0, -1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 0, 110, 1, 0, 0, 0, 1, -1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 0, 110, 1, 0, 0, 0, -1, -1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 10, 1, 0, 0, 0, 0, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 10, 1, 0, 0, 0, 0, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 0, 10, 1, 0, 0, 0, 0, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 0, 10, 1, 0, 0, 0, 1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 0, 10, 1, 0, 0, 0, -1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 105, 1, 0, 0, 0, -1, -1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 100, 1, 0, 0, 0, -1, -1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 95, 1, 0, 0, 0, -1, -1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 90, 1, 0, 0, 0, -1, -1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 85, 1, 0, 0, 0, -1, -1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 105, 1, 0, 0, 0, -1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 100, 1, 0, 0, 0, -1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 95, 1, 0, 0, 0, -1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 90, 1, 0, 0, 0, -1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 85, 1, 0, 0, 0, -1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 80, 1, 0, 0, 0, -1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 75, 1, 0, 0, 0, -1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 70, 1, 0, 0, 0, -1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 65, 1, 0, 0, 0, -1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 60, 1, 0, 0, 0, -1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 55, 1, 0, 0, 0, -1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 50, 1, 0, 0, 0, -1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 45, 1, 0, 0, 0, -1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 40, 1, 0, 0, 0, -1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, 20, 35, 1, 0, 0, 0, -1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 50, 1, 0, 0, 0, 1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 60, 1, 0, 0, 0, 1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 70, 1, 0, 0, 0, 1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 80, 1, 0, 0, 0, 1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 90, 1, 0, 0, 0, 1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 100, 1, 0, 0, 0, 1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 95, 1, 0, 0, 0, 1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 85, 1, 0, 0, 0, 1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 75, 1, 0, 0, 0, 1, 1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 50, 1, 0, 0, 0, 1, -1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 55, 1, 0, 0, 0, 1, -1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 60, 1, 0, 0, 0, 1, -1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 65, 1, 0, 0, 0, 1, -1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 70, 1, 0, 0, 0, 1, -1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 75, 1, 0, 0, 0, 1, -1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 80, 1, 0, 0, 0, 1, -1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 85, 1, 0, 0, 0, 1, -1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 90, 1, 0, 0, 0, 1, -1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 95, 1, 0, 0, 0, 1, -1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 100, 1, 0, 0, 0, 1, -1, jawwidth=20))
            # predefined_grasps.append(rtq85.approachAt(0, -20, 45, 1, 0, 0, 0, 1, -1, jawwidth=20))

            armname = "lft"
            obj = cm.CollisionModel(objinit="./objects/domino.stl")

            point = np.array([400,-100,-999])
            distance = -1e12
            for goalelement in goallist[0]:
                if np.linalg.norm(goalelement[:3, 3] - point) > distance:
                    distance = np.linalg.norm(goalelement[:3, 3] - point)
                    objmat4_initial = goalelement
                    objpos_initial = goalelement[:3, 3]
                    objrot_initial = goalelement[:3, :3]

            obj_initial = copy.deepcopy(obj)
            obj_initial.setColor(1, 0, 0, .5)
            obj_initial.setMat(base.pg.np4ToMat4(objmat4_initial))
            obj_initial.reparentTo(base.render)
            obj_initial.showLocalFrame()

            result = sg.getsymmetricgoals(rm.homobuild(xydominogoals[goalcounter[0]][0],xydominogoals[goalcounter[0]][1]))
            objpos_finallist = result[0]
            objrot_finallist = result[1]
            finished_obj_list = []

            for k in range(len(finishedlist[0])):
                obj_finished = copy.deepcopy(obj)
                finishedmat4 = rm.homobuild(finishedlist[0][k][0], finishedlist[0][k][1])
                obj_finished.setMat(base.pg.np4ToMat4(finishedmat4))
                finished_obj_list.append(obj_finished)
            isfound = False
            for i in range(4):
                objpos_final = objpos_finallist[i]
                objrot_final = objrot_finallist[i]
                objmat4_final = rm.homobuild(objpos_final, objrot_final)
                obj_final = copy.deepcopy(obj)
                obj_final.setColor(0, 0, 1, .5)
                obj_final.setMat(base.pg.np4ToMat4(objmat4_final))
                obj_final.reparentTo(base.render)
                obj_final.showLocalFrame()

                candidateidlist = sg.findsharedgrasps(objpos_initial, objrot_initial, objpos_final, objrot_final,
                                                   predefined_grasps, obj, rbt, rbtmg, rbtball, armname, hndfa,
                                                   obscmlist + finished_obj_list)
                # return task.done
                print(candidateidlist)
                if len(candidateidlist) == 0:
                    continue

                grasppair = []
                for candidateid in candidateidlist:
                    prejawwidth, prehndfc, prehndmat4 = predefined_grasps[candidateid]
                    hndmat4_initial = np.dot(objmat4_initial, prehndmat4)
                    eepos_initial = rm.homotransform(objmat4_initial, prehndfc)[:3]
                    eerot_initial = hndmat4_initial[:3, :3]
                    hndmat4_final = np.dot(objmat4_final, prehndmat4)
                    eepos_final = rm.homotransform(objmat4_final, prehndfc)[:3]
                    eerot_final = hndmat4_final[:3, :3]
                    grasppair.append([[eepos_initial, eerot_initial], [eepos_final, eerot_final]])

                for j in range(len(grasppair)):
                    eepos_initial, eerot_initial = grasppair[0][0]
                    start = rbt.numik(eepos_initial, eerot_initial, armname)
                    rbt.movearmfk(start, armname=armname)

                    objrelpos, objrelrot = rbt.getinhandpose(objpos_initial, objrot_initial, armname)

                    eepos_final, eerot_final = grasppair[0][1]
                    goal = rbt.numik(eepos_final, eerot_final, armname)
                    rbt.movearmfk(goal, armname=armname)
                    cdchecker = cdck.CollisionCheckerBall(rbtball)
                    ctcallback = ctcb.CtCallback(base, rbt, cdchecker=cdchecker, ctchecker=None, armname=armname)

                    pickupprimitive = ctcallback.getLinearPrimitive(start, [0, 0, 1], 150, [obj], [[objrelpos, objrelrot]],
                                                                    obscmlist, type="source")
                    placedownprimitive = ctcallback.getLinearPrimitive(goal, [0, 0, 1], 150, [obj], [[objrelpos, objrelrot]],
                                                                       obscmlist, type="sink")
                    if len(pickupprimitive) == 0 or len(placedownprimitive) == 0:
                        continue

                    rbt.movearmfk(pickupprimitive[-1], armname=armname)

                    objpos, objrot = rbt.getworldpose(objrelpos, objrelrot, armname)
                    objmat4 = rm.homobuild(objpos, objrot)
                    obj = copy.deepcopy(obj)
                    obj.setColor(1.0, 1.0, 1.0, .5)
                    obj.setMat(base.pg.np4ToMat4(objmat4))
                    obj.reparentTo(base.render)

                    rbt.movearmfk(placedownprimitive[0], armname=armname)

                    objpos, objrot = rbt.getworldpose(objrelpos, objrelrot, armname)
                    objmat4 = rm.homobuild(objpos, objrot)
                    obj = copy.deepcopy(obj)
                    obj.setColor(1.0, 1.0, 1.0, .5)
                    obj.setMat(base.pg.np4ToMat4(objmat4))
                    obj.reparentTo(base.render)

                    rbt.goinitpose()
                    # armjnts = uc.getjnts(armname)
                    # rbt.movearmfk(armjnts, armname)

                    planner = rrtc.RRTConnect(start=rbt.getarmjnts(armname), goal=pickupprimitive[-1], ctcallback=ctcallback,
                                              starttreesamplerate=30,
                                              endtreesamplerate=30, expanddis=20,
                                              maxiter=500, maxtime=3)
                    path1, samples = planner.planning(obscmlist+[obj_initial]+finished_obj_list)
                    if path1 is False:
                        rbt.goinitpose()
                        continue
                    smoother = sm.Smoother()
                    path1 = smoother.pathsmoothing(path1, planner)

                    planner = rrtc.RRTConnect(start=pickupprimitive[-1], goal=placedownprimitive[0], ctcallback=ctcallback,
                                              starttreesamplerate=30,
                                              endtreesamplerate=30, expanddis=20,
                                              maxiter=500, maxtime=3)
                    path, samples = planner.planninghold([obj], [[objrelpos, objrelrot]], obscmlist+finished_obj_list)
                    if path is False:
                        rbt.goinitpose()
                        continue
                    smoother = sm.Smoother()
                    path = smoother.pathsmoothinghold(path, planner)

                    pathmoveto = path1 + pickupprimitive[::-1]
                    pathpick = pickupprimitive + path + placedownprimitive
                    pathmoveaway = placedownprimitive[::-1]
                    path = [pathmoveto, pathpick, pathmoveaway]

                    # draw animation
                    def motionanime(rbtmnp, objmnp, motioncounter, rbt, path, armname, rbtmg, obj, objrelmat, task):
                        pathall = path[0]+path[1]+path[2]
                        if motioncounter[0] < len(pathall):
                            if rbtmnp[0] is not None:
                                rbtmnp[0].detachNode()
                            if objmnp[0] is not None:
                                objmnp[0].detachNode()
                            pose = pathall[motioncounter[0]]
                            rbt.movearmfk(pose, armname)
                            rbtmnp[0] = rbtmg.genmnp(rbt)
                            rbtmnp[0].reparentTo(base.render)
                            if motioncounter[0] > len(path[0]) and motioncounter[0] < len(path[0])+len(path[1]):
                                objmnp[0] = copy.deepcopy(obj)
                                objmnp[0].setColor(.8, .6, .3, .5)
                                objpos, objrot = rbt.getworldpose(objrelmat[0], objrelmat[1], armname)
                                objmat4 = rm.homobuild(objpos, objrot)
                                objmnp[0].setMat(base.pg.np4ToMat4(objmat4))
                                objmnp[0].reparentTo(base.render)
                            motioncounter[0] += 1
                        else:
                            return task.done
                        return task.again
                    rbtmnp = [None]
                    objmnp = [None]
                    motioncounter = [0]
                    taskMgr.doMethodLater(0.05, motionanime, "motionanime",
                                          extraArgs=[rbtmnp, objmnp, motioncounter, rbt, path, armname, rbtmg, obj,
                                                     [objrelpos, objrelrot]],
                                          appendTask=True)

                    # uc.opengripper(armname,80)
                    # uc.movejntssgl_cont(pathmove, armname)
                    # uc.closegripper(armname)
                    # time.sleep(1)
                    # uc.movejntssgl_cont(pathpick, armname)
                    # uc.attachfirm(rbt, upthreshold=10, armname = armname)
                    # uc.opengripper(armname, 80)
                    # uc.movejntssgl_cont(placedownprimitive[::-1], armname)

                    isfound = True
                    finishedlist[0].append([objpos_final, objrot_final])
                    break
                if isfound:
                    break
            pserv.poselist = []
            goallist[0] = []
            isgoalobtained[0] = False
            goalcounter[0] += 1
            if goalcounter[0] == len(xydominogoals):
                return task.done

        return task.again

    taskMgr.doMethodLater(0.05, update, "update",
                          extraArgs=[pserv, isgoalobtained, goallist, obj, rtq85, rbtball, finishedlist, xydominogoals, goalcounter],
                          appendTask=True)


    base.run()
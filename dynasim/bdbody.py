from panda3d.bullet import BulletRigidBodyNode
from panda3d.bullet import BulletTriangleMesh
from panda3d.bullet import BulletTriangleMeshShape
from panda3d.bullet import BulletConvexHullShape
from panda3d.core import TransformState, Mat4, Vec3, GeomVertexRewriter
import copy
import environment.collisionmodel as cm
import numpy as np
import utiltools.robotmath as rm

class BDTriangleBody(BulletRigidBodyNode):

    def __init__(self, obj, mass=.3, restitution=0, allowdeactivation = False, allowccd = True, friction=.2, dynamic=False, name="rbd"):
        """

        :param obj: could be itself (copy), or an instance of collision model
        :param mass:
        :param restitution: bounce parameter
        :param friction:
        :param name:

        author: weiwei
        date: 20190626
        """

        super().__init__(name)

        if isinstance(obj, cm.CollisionModel):
            self.objcm = obj
            self.setMass(mass)
            self.setRestitution(restitution)
            self.setFriction(friction)
            self.setLinearDamping(.3)
            self.setAngularDamping(.3)
            if allowdeactivation:
                self.setDeactivationEnabled(True)
                self.setLinearSleepThreshold(10)
                self.setAngularSleepThreshold(10)
            else:
                self.setDeactivationEnabled(False)
            if allowccd:
                self.setCcdMotionThreshold(1e-7);
                self.setCcdSweptSphereRadius(0.5);

            gnd = obj.objnp.find("+GeomNode")
            geom = copy.deepcopy(gnd.node().getGeom(0))
            vdata = geom.modifyVertexData()
            vertrewritter = GeomVertexRewriter(vdata, 'vertex')
            while not vertrewritter.isAtEnd():
                v = vertrewritter.getData3f()
                vertrewritter.setData3f(v[0]-obj.com[0], v[1]-obj.com[1], v[2]-obj.com[2])
            geomtf = gnd.getTransform(base.render)
            geombmesh = BulletTriangleMesh()
            geombmesh.addGeom(geom)
            bulletshape = BulletTriangleMeshShape(geombmesh, dynamic=dynamic)
            bulletshape.setMargin(1e-6)

            self.addShape(bulletshape, geomtf)
            m4homo = geomtf.getMat()
            v3pos = m4homo.xformPoint(Vec3(self.objcm.com[0], self.objcm.com[1], self.objcm.com[2]))
            np4homo = base.pg.mat4ToNp(m4homo)
            np3pos = base.pg.v3ToNp(v3pos)
            np4homo[:3, 3] = np3pos
            self.homomat = np4homo
            self.setTransform(TransformState.makeMat(base.pg.np4ToMat4(self.homomat)))

        elif isinstance(obj, BDTriangleBody):
            self.objcm = obj.objcm
            self.setMass(obj.getMass())
            self.setRestitution(obj.restitution)
            self.setFriction(obj.friction)
            self.setLinearDamping(.3)
            self.setAngularDamping(.3)
            if allowdeactivation:
                self.setDeactivationEnabled(True)
                self.setLinearSleepThreshold(10)
                self.setAngularSleepThreshold(10)
            else:
                self.setDeactivationEnabled(False)
            if allowccd:
                self.setCcdMotionThreshold(1e-7);
                self.setCcdSweptSphereRadius(0.5);
            self.homomat = obj.homomat
            self.setTransform(TransformState.makeMat(base.pg.np4ToMat4(self.homomat)))
            self.addShape(obj.getShape(0), obj.getShapeTransform(0))

    def gethomomat(self):
        m4homo = self.getTransform().getMat()
        v3pos = m4homo.xformPoint(Vec3(-self.objcm.com[0], -self.objcm.com[1], -self.objcm.com[2]))
        np4homo = base.pg.mat4ToNp(m4homo)
        np3pos = base.pg.v3ToNp(v3pos)
        np4homo[:3, 3] = np3pos
        return np4homo

    def sethomomat(self, np4homo):
        # oldnp4homo = self.gethomomat()
        np4pos = rm.homotransform(np4homo, self.objcm.com)
        # nprot = np.dot(np4homo[:3,:3],oldnp4homo[:3,:3])
        # nppos = oldnp4homo[:3, 3]+np4pos
        nprot = np4homo[:3,:3]
        nppos = np4pos
        self.setTransform(TransformState.makeMat(base.pg.np4ToMat4(rm.homobuild(nppos, nprot))))

    def copy(self):
        return BDTriangleBody(self)

class BDConvexBody(BulletRigidBodyNode):

    def __init__(self, obj, mass=.3, restitution=0, allowdeactivation = False, allowccd = True, friction=.2, name="rbd"):
        """

        :param obj: could be itself (copy), or an instance of collision model
        :param mass:
        :param restitution: bounce parameter
        :param friction:
        :param name:

        author: weiwei
        date: 20190626
        """

        super().__init__(name)

        if isinstance(obj, cm.CollisionModel):
            self.objcm = obj
            self.setMass(mass)
            self.setRestitution(restitution)
            self.setFriction(friction)
            self.setLinearDamping(.3)
            self.setAngularDamping(.3)
            if allowdeactivation:
                self.setDeactivationEnabled(True)
                self.setLinearSleepThreshold(10)
                self.setAngularSleepThreshold(10)
            else:
                self.setDeactivationEnabled(False)
            if allowccd:
                self.setCcdMotionThreshold(1e-7);
                self.setCcdSweptSphereRadius(0.5);

            gnd = obj.objnp.find("+GeomNode")
            geom = copy.deepcopy(gnd.node().getGeom(0))
            vdata = geom.modifyVertexData()
            vertrewritter = GeomVertexRewriter(vdata, 'vertex')
            while not vertrewritter.isAtEnd():
                v = vertrewritter.getData3f()
                vertrewritter.setData3f(v[0]-obj.com[0], v[1]-obj.com[1], v[2]-obj.com[2])
            geomtf = gnd.getTransform(base.render)
            bulletshape = BulletConvexHullShape()
            bulletshape.addGeom(geom, geomtf)
            bulletshape.setMargin(1e-6)

            self.addShape(bulletshape, geomtf)
            m4homo = geomtf.getMat()
            v3pos = m4homo.xformPoint(Vec3(self.objcm.com[0], self.objcm.com[1], self.objcm.com[2]))
            np4homo = base.pg.mat4ToNp(m4homo)
            np3pos = base.pg.v3ToNp(v3pos)
            np4homo[:3, 3] = np3pos
            self.homomat = np4homo
            self.setTransform(TransformState.makeMat(base.pg.np4ToMat4(self.homomat)))

        elif isinstance(obj, BDConvexBody):
            self.objcm = obj.objcm
            self.setMass(obj.getMass())
            self.setRestitution(obj.restitution)
            self.setFriction(obj.friction)
            self.setLinearDamping(.3)
            self.setAngularDamping(.3)
            if allowdeactivation:
                self.setDeactivationEnabled(True)
                self.setLinearSleepThreshold(10)
                self.setAngularSleepThreshold(10)
            else:
                self.setDeactivationEnabled(False)
            if allowccd:
                self.setCcdMotionThreshold(1e-7);
                self.setCcdSweptSphereRadius(0.5);
            self.homomat = obj.homomat
            self.setTransform(TransformState.makeMat(base.pg.np4ToMat4(self.homomat)))
            self.addShape(obj.getShape(0), obj.getShapeTransform(0))

    def gethomomat(self):
        m4homo = self.getTransform().getMat()
        v3pos = m4homo.xformPoint(Vec3(-self.objcm.com[0], -self.objcm.com[1], -self.objcm.com[2]))
        np4homo = base.pg.mat4ToNp(m4homo)
        np3pos = base.pg.v3ToNp(v3pos)
        np4homo[:3, 3] = np3pos
        return np4homo

    def sethomomat(self, np4homo):
        # oldnp4homo = self.gethomomat()
        np4pos = rm.homotransform(np4homo, self.objcm.com)
        # nprot = np.dot(np4homo[:3,:3],oldnp4homo[:3,:3])
        # nppos = oldnp4homo[:3, 3]+np4pos
        nprot = np4homo[:3,:3]
        nppos = np4pos
        self.setTransform(TransformState.makeMat(base.pg.np4ToMat4(rm.homobuild(nppos, nprot))))

    def copy(self):
        return BDConvexBody(self)
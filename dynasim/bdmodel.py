import trimesh
import trimesh.sample as sample
import copy
import math
import os
import numpy as np
import utiltools.robotmath as rm
from panda3d.core import CollisionNode
from panda3d.core import CollisionBox
from panda3d.core import CollisionSphere
from panda3d.core import TransparencyAttrib
from panda3d.core import NodePath
import environment.collisionmodel as cm
import dynasim.bdbody as bdb

class BDModel(object):
    """
    load an object as a bullet dynamics model

    author: weiwei
    date: 20190627
    """

    def __init__(self, objinit, mass=None, betransparency = True, cmtype = "box", radius=None,
                 restitution=0, allowdeactivation = False, allowccd = True, friction=.2, dynamic=False,
                 shapetype = "convex", name="bdm"):
        """

        :param objinit:
        :param mass:
        :param betransparency:
        :param cmtype:
        :param radius:
        :param restitution:
        :param allowdeactivation:
        :param allowccd:
        :param friction:
        :param dynamic:
        :param shapetype: string like "convex", "triangle"
        :param name:
        """

        if isinstance(objinit, BDModel):
            self.__objcm = copy.deepcopy(objinit.objcm)
            self.__objbdb = objinit.objbdb.copy()
            base.physicsworld.attach(self.__objbdb)
        else:
            if cmtype not in ["box", "ball"]:
                raise Exception("Wrong Collision Model type name.")
            self.__objcm = cm.CollisionModel(objinit, betransparency, cmtype, radius, name)
            if mass is None:
                mass = 0
            if shapetype is "convex":
                self.__objbdb = bdb.BDConvexBody(self.objcm, mass, restitution, allowdeactivation, allowccd, friction, name="bdm")
            elif shapetype is "triangle":
                self.__objbdb = bdb.BDTriangleBody(self.objcm, mass, restitution, allowdeactivation, allowccd, friction, dynamic=dynamic, name="bdm")
            base.physicsworld.attach(self.__objbdb)

    @property
    def objcm(self):
        # read-only property
        return self.__objcm

    @property
    def objbdb(self):
        # read-only property
        return self.__objbdb

    def setColor(self, r, g, b, a):
        self.__objcm.objnp.setColor(r, g, b, a)

    def clearColor(self):
        self.__objcm.__objnp.clearColor()

    def getColor(self):
        return self.__objcm.objnp.getColor()

    def setPos(self, x, y, z):
        # self.__objcm.objnp.setPos(x, y, z)
        dbmatnp = self.__objbdb.gethomomat()
        dbmatnp[:3,3] = np.array([x,y,z])
        self.__objbdb.sethomomat(dbmatnp)
        self.__objcm.objnp.setMat(base.pg.np4ToMat4(dbmatnp))

    def getPos(self, rel = None):
        if rel is None:
            return self.__objcm.objnp.getPos()
        else:
            return self.__objcm.objnp.getPos(rel)

    def setMat(self, pandamat4):
        self.__objbdb.sethomomat(base.pg.mat4ToNp(pandamat4))
        self.__objcm.objnp.setMat(pandamat4)
        # self.__objcm.objnp.setMat(base.pg.np4ToMat4(self.objbdb.gethomomat()))

    def sethomomat(self, npmat4):
        self.__objbdb.sethomomat(npmat4)
        self.__objcm.sethomomat(npmat4)

    def setRPY(self, roll, pitch, yaw):
        """
        set the pose of the object using rpy

        :param roll: degree
        :param pitch: degree
        :param yaw: degree
        :return:

        author: weiwei
        date: 20190513
        """

        currentmat = self.__objbdb.gethomomat()
        currentmatnp = base.pg.mat4ToNp(currentmat)
        newmatnp = rm.euler_matrix(roll, pitch, yaw, axes="sxyz")
        self.setMat(base.pg.npToMat4(newmatnp, currentmatnp[:,3]))

    def getRPY(self):
        """
        get the pose of the object using rpy

        :return: [r, p, y] in degree

        author: weiwei
        date: 20190513
        """

        currentmat = self.objcm.getMat()
        currentmatnp = base.pg.mat4ToNp(currentmat)
        rpy = rm.euler_from_matrix(currentmatnp[:3,:3], axes="sxyz")
        return np.array([rpy[0], rpy[1], rpy[2]])

    def getMat(self, rel=None):
        return self.objcm.getMat(rel)

    def gethomomat(self, rel=None):
        pandamat4 = self.getMat(rel)
        return base.pg.mat4ToNp(pandamat4)

    def setMass(self, mass):
        self.__objbdb.setMass(mass)

    def reparentTo(self, objnp):
        """
        objnp must be base.render

        :param objnp:
        :return:

        author: weiwei
        date: 20190627
        """

        # if isinstance(objnp, cm.CollisionModel):
        #     self.__objcm.objnp.reparentTo(objnp.objnp)
        # elif isinstance(objnp, NodePath):
        #     self.__objcm.objnp.reparentTo(objnp)
        # else:
        #     print("NodePath.reparent_to() argument 1 must be environment.CollisionModel or panda3d.core.NodePath")
        if objnp is not base.render:
            print("This bullet dynamics model doesnt support to plot to non base.render nodes!")
            raise ValueError("Value Error!")
        else:
            self.__objcm.objnp.reparentTo(objnp)
        # self.setMat(self.__objcm.getMat())
        # print(self.objbdb.gethomomat())
        self.__objcm.objnp.setMat(base.pg.np4ToMat4(self.objbdb.gethomomat()))

    def removeNode(self):
        self.__objcm.objnp.removeNode()
        base.physicsworld.remove(self.__objbdb)

    def detachNode(self):
        self.__objcm.objnp.detachNode()

    def showcn(self):
        # reattach to bypass the failure of deepcopy
        self.__cdnp.removeNode()
        self.__cdnp = self.__objnp.attachNewNode(self.__cdcn)
        self.__cdnp.show()

    def showLocalFrame(self):
        self.__localframe = base.pggen.genAxis()
        self.__localframe.reparentTo(self.objnp)

    def unshowLocalFrame(self):
        if self.__localframe is not None:
            self.__localframe.removeNode()
            self.__localframe = None

    def unshowcn(self):
        self.__cdnp.hide()

    def copy(self):
        return BDModel(self)

if __name__=="__main__":
    import os
    import numpy as np
    import utiltools.robotmath as rm
    import pandaplotutils.pandactrl as pc
    import random

    base = pc.World(camp=[1000,300,1000], lookatp=[0,0,0], toggledebug=False)
    base.setFrameRateMeter(True)

    this_dir, this_filename = os.path.split(__file__)
    objpath = os.path.join(this_dir, "objects", "block.stl")
    bunnycm = BDModel(objpath, mass=1, shapetype="convex")

    objpath2 = os.path.join(this_dir, "objects", "bowlblock.stl")
    bunnycm2 = BDModel(objpath2, mass=0, shapetype="triangle", dynamic=False)
    bunnycm2.setColor(0, 0.7, 0.7, 1.0)
    # bunnycm2.reparentTo(base.render)
    bunnycm2.setPos(0,0,0)
    base.attachRUD(bunnycm2)

    def update(bunnycm, task):
        if base.inputmgr.keyMap['space'] is True:
            for i in range(300):
                bunnycm1 = bunnycm.copy()
                bunnycm1.setMass(.1)
                # bunnycm1.setColor(0.7, 0, 0.7, 1.0)
                bunnycm1.setColor(random.random(), random.random(), random.random(), 1.0)
                # bunnycm1.reparentTo(base.render)
                # rotmat = rm.rodrigues([0,0,1], 15)
                rotmat = rm.euler_matrix(0,0,15)
                z = math.floor(i/100)
                y = math.floor((i-z*100)/10)
                x = i-z*100-y*10
                print(x,y,z,"\n")
                bunnycm1.setMat(base.pg.npToMat4(rotmat, np.array([x*15-70, y*15-70, 150+z*15])))
                base.attachRUD(bunnycm1)
        base.inputmgr.keyMap['space'] = False
        return task.cont

    base.pggen.plotAxis(base.render)
    taskMgr.add(update, "addobject", extraArgs=[bunnycm], appendTask=True)

    base.run()

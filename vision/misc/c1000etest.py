import cv2

cam = cv2.VideoCapture(2)
cam.set(cv2.CAP_PROP_FRAME_WIDTH, 3840)
cam.set(cv2.CAP_PROP_FRAME_HEIGHT, 2160)
cam.set(cv2.CAP_PROP_FOCUS, 0)

while True:
    _, img = cam.read()
    cv2.imshow("test", img)
    cv2.waitKey(1)